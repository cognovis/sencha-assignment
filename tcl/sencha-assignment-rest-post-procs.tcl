ad_library {
    Rest POST Procedures for the sencha-tables package
    
    @author malte.sussdorff@cognovis.de
}



ad_proc -public im_rest_post_object_type_sencha_freelance_assignment {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for GET rest calls which assigns freelancer to project task
} {

    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    return [array get hash_array]
   

}



ad_proc -public im_rest_post_object_type_sencha_request_participation {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for GET rest calls which assigns freelancer to project task
} {
    

    # We need locale to return translated errors (if any)
    set locale [lang::user::locale -user_id $rest_user_id]

    # We need variable to collect id of objects with potential errors
    set object_ids [list]

    set assignment_id 0
    set problem ""
    set project_id ""
    set obj_ctr 0
    array set parsed_json [util::json::parse $content]
    set json_list $parsed_json(_array_)

    set newly_created_assignments [list]

    set project_status_id ""
    set potential_subcategories [im_sub_categories [im_project_status_potential]]
    set potential_p 0

    foreach single_freelancer_data $json_list {
	
        array set freelancer_dataset [lindex $single_freelancer_data 1]
	
        set freelance_package_id $freelancer_dataset(package_id)
        set freelancer_id $freelancer_dataset(freelancer_id)
        set comment $freelancer_dataset(comment)
        set assignment_type_id $freelancer_dataset(task_type_id)
        set assignment_units $freelancer_dataset(units)
        set rate $freelancer_dataset(rate)
        set deadline $freelancer_dataset(deadline)
        set uom_id $freelancer_dataset(uom)

        set freelancer_name ""
        db_1row freelancer_info "select coalesce(first_names, last_name) as freelancer_name from cc_users where user_id =:freelancer_id"

    	if {$project_status_id eq ""} {
    	    set project_status_id [db_string get_project_status_id "select project_status_id from im_projects p,im_freelance_packages fp where fp.project_id=p.project_id and freelance_package_id = :freelance_package_id" -default ""]
    	    if {[lsearch -exact $potential_subcategories $project_status_id] > -1} {
    		    set potential_p 1
    	    }
    	}


    	# Potential projects have assignment status of created.
    	if {$potential_p} {
    	    set assignment_status_id 4220
    	} else {
    	    # Assignment status should be created
    	    set assignment_status_id 4221
    	}

        set freelance_package_name ""
        db_1row package_info "select freelance_package_name from im_freelance_packages where freelance_package_id=:freelance_package_id"

        catch {
            set assignment_id [im_freelance_assignment_create \
    			       -freelance_package_id $freelance_package_id \
    			       -assignee_id $freelancer_id \
    			       -assignment_type_id $assignment_type_id \
    			       -assignment_status_id $assignment_status_id \
    			       -assignment_units $assignment_units \
    			       -end_date $deadline \
    			       -rate $rate \
    			       -uom_id $uom_id \
    			       -assignment_comment $comment \
    			       -ignore_min_price_p 1
                ]
            lappend newly_created_assignments $assignment_id
        } errmsg

        # Assignment was created if assignment_id is not 0
        if {$assignment_id eq 0} {
            set problem [lang::message::lookup $locale "sencha-assignment.could_not_create_assignment"]
            sencha_errors::add_error -object_id $freelancer_id -field "$freelancer_name ($freelance_package_name): $problem" -problem $problem
            lappend object_ids $freelancer_id
        }

    	if {!$potential_p} {
	        # this is not a potenial project, so immediately send the request to the freelancer
            set errmsg ""
            catch {
                im_freelance_trans_send_request -assignment_id $assignment_id
            } errmsg

            if {[string length $errmsg] > 0} {
                set problem [lang::message::lookup $locale "sencha-assignment.could_not_send_email"]
                sencha_errors::add_error -object_id $assignment_id -field "$freelancer_name ($freelance_package_name): $problem" -problem "$errmsg"
                lappend object_ids $assignment_id
            }
    	}
    }

    # Display errors or warnings (if any). This won't do anything in case of no errors
    sencha_errors::display_all_existing_errors_and_warnings -object_ids $object_ids -modal_title $problem

    set hash_array(rest_oid) $project_id

    return [array get hash_array]
}


ad_proc -public im_rest_post_object_type_sencha_create_multiple_assignments {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for GET rest calls which assigns freelancer to project task
} {

    # We need locale to return translated errors (if any)
    set locale [lang::user::locale -user_id $rest_user_id]

    # We need variable to collect id of objects with potential errors
    set object_ids [list]

    set problem ""
    set project_id ""
    set obj_ctr 0
    set assignment_id 0

    array set parsed_json [util::json::parse $content]
    set json_list $parsed_json(_array_)

    set newly_created_assignments [list]
    set freelancers_already_taken_care_of [list]

    # Do not send out mails for potential projects
    set project_status_id ""
    set potential_subcategories [im_sub_categories [im_project_status_potential]]
    set potential_p 0

    foreach single_freelancer_data $json_list {
        
        array set freelancer_dataset [lindex $single_freelancer_data 1]

        set freelancer_id $freelancer_dataset(freelancer_id)
        set assignment_type_id $freelancer_dataset(task_type_id)
        set target_language_id $freelancer_dataset(target_language_id)
        set merged_freelancer_id_language_id "$freelancer_id-$target_language_id"

        set freelancer_name ""
        db_1row freelancer_info "select coalesce(first_names, last_name) as freelancer_name from cc_users where user_id =:freelancer_id"


        if {[lsearch -exact $freelancers_already_taken_care_of $merged_freelancer_id_language_id] == -1} {

            set freelancer_has_multiple_assignments_attempts_p 0
            set count_freelancer_attempts $freelancer_dataset(countPotentialAssignments)
            if {$count_freelancer_attempts > 0} {
                set freelancer_has_multiple_assignments_attempts_p 1
            }

            if {$freelancer_has_multiple_assignments_attempts_p} {
                set task_ids $freelancer_dataset(multipleTasksIds)
                set multiple_package_ids $freelancer_dataset(multiplePackageIds)
                set new_freelance_package_id [im_freelance_package_create -trans_task_ids $task_ids -package_type_id $assignment_type_id]
                set freelance_package_id $new_freelance_package_id
                # also delete old packages
                db_dml delete_packages "delete from im_freelance_packages where freelance_package_id in([ns_dbquotelist $multiple_package_ids])"
            } else {
                set freelance_package_id $freelancer_dataset(package_id)
            }

            set freelance_package_name ""
            db_1row package_info "select freelance_package_name from im_freelance_packages where freelance_package_id=:freelance_package_id"

            set comment $freelancer_dataset(comment)
        
            set assignment_units $freelancer_dataset(units)
            set rate $freelancer_dataset(rate)
            set deadline $freelancer_dataset(deadline)
            set uom_id $freelancer_dataset(uom)
            set create_po_p $freelancer_dataset(create_po_p)

            if {$create_po_p} {
                set assignment_status_id 4222
            } else {
                set assignment_status_id 4220
            }

	    if {$project_status_id eq ""} {
		    set project_status_id [db_string get_project_status_id "select project_status_id from im_projects p,im_freelance_packages fp where fp.project_id=p.project_id and freelance_package_id = :freelance_package_id" -default ""]
		    if {[lsearch -exact $potential_subcategories $project_status_id] > -1} {
		        # Project status is 'potential'
                set potential_p 1
		    }
	    }

	    # Overwrite assignment status id. Can't provide a purchase order for potential projects
	    if {$potential_p} {
		    set assignment_status_id 4220
		    set create_po_p 0
	    }

       catch {
           set assignment_id [im_freelance_assignment_create \
    				   -freelance_package_id $freelance_package_id \
    				   -assignee_id $freelancer_id \
    				   -assignment_type_id $assignment_type_id \
    				   -assignment_status_id $assignment_status_id \
    				   -assignment_units $assignment_units \
    				   -end_date $deadline \
    				   -rate $rate \
    				   -uom_id $uom_id \
    				   -assignment_comment $comment \
    				   -ignore_min_price_p 1 
                         
            ]
        } errmsg

        # Assignment was created if assignment_id is not 0
        if {$assignment_id eq 0} {

            set problem [lang::message::lookup $locale "sencha-assignment.could_not_create_assignment"]
            sencha_errors::add_error -object_id $freelancer_id -field "$freelancer_name ($freelance_package_name): $problem" -problem $errmsg
            lappend object_ids $freelancer_id

        } else {

            if {!$potential_p} {
                
                set errmsg ""
    		    # Send the notification for the assignment
                catch {
    		        im_freelance_trans_send_assignment -assignment_id $assignment_id 
                } errmsg
                
                if {[string length $errmsg] > 0} {
                    set problem [lang::message::lookup $locale "sencha-assignment.could_not_send_email"]
                    sencha_errors::add_error -object_id $assignment_id -field "$freelancer_name ($freelance_package_name): $problem" -problem "$errmsg"
                    lappend object_ids $assignment_id
                }
                
                if {$create_po_p} {
    		        # Create the PO and send it.
                    set errmsg ""
                    set invoice_id 0
                    catch {
                        set invoice_id [im_freelance_create_purchase_orders -assignment_id $assignment_id -user_id $rest_user_id]
    		            im_invoice_send_invoice_mail -invoice_id $invoice_id -recipient_id $freelancer_id -from_addr [party::email -party_id [auth::get_user_id]]
                    }
                    if {$invoice_id eq 0} {
                        set problem [lang::message::lookup $locale "sencha-assignment.could_not_create_po"]
                        sencha_errors::add_error -object_id $assignment_id -field "$freelancer_name ($freelance_package_name): $problem" -problem $problem            
                        lappend object_ids $assignment_id
                    }
                }

            } else {
                set invoice_id false
            }
    	    
    	    if {$deadline eq ""} {
    		    im_freelance_calculate_deadline -assignment_id $assignment_id -update_deadline_p 1
    	    }

            lappend newly_created_assignments $assignment_id

            if {[lsearch -exact $freelancers_already_taken_care_of $merged_freelancer_id_language_id] == -1} {
                lappend freelancers_already_taken_care_of $merged_freelancer_id_language_id
            }

            }
        }

    }

    # Display errors or warnings (if any). This won't do anything in case of no errors
    sencha_errors::display_all_existing_errors_and_warnings -object_ids $object_ids -modal_title $problem

    set any_newly_created_assignment_id [lindex $newly_created_assignments 0]
    set hash_array(rest_oid) $any_newly_created_assignment_id
    #set hash_array(note_id) "5645"

    return [array get hash_array]
   

}

ad_proc -public im_rest_post_object_type_sencha_update_freelancer_ratings {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for GET rest calls to get all created freelancer ratings for given project_id

    @param assignment_id of the assignment for which we want to update existing ratings
} {

    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]
    set assignment_id $hash_array(assignment_id)
    
    set comment $hash_array(comment)
    
    # Frist we get report_id
    set report_id [db_string get_report_id "select report_id from im_assignment_quality_reports where assignment_id =:assignment_id" -default ""]
    
    if {$report_id ne ""} {
        # Update ratings
        foreach single_rating_json_obj [lindex $hash_array(ratings) 1] {
            array set rating_dataset [lindex $single_rating_json_obj 1]
            set id_category_rated $rating_dataset(id_category)
            set id_value_rated $rating_dataset(value)
            db_dml update_ratings "Update im_assignment_quality_report_ratings set quality_level_id =:id_value_rated where report_id =:report_id and quality_type_id=:id_category_rated"
        }
        # Saving comment
        db_dml update_comment "update im_assignment_quality_reports set comment =:comment where report_id=:report_id"
    
    }



    set result "{\"success\": true}"
    im_rest_doc_return 200 "application/json" $result
    return

}
