# /packages/sencha-assignment/tcl/sencha-assignment.tcl
#

ad_library {
    Common callbacks for sencha-assignment package (mainly it is freelacner select screen)
    @author malte.sussdorff@cognovis.de
}


ad_proc -public -callback im_freelance_packages_after_create {
    {-task_ids:required}
} {
    Callback that is executed after freelance packages are auto-created from tasks. 
    This happens before Assignments Modal is opened
} -

ad_proc -public -callback im_project_after_quote2order -impl sencha_assignment_request {
    {-project_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Send the request to all created assignments for this project and set them to requested
} {
    # Get all created assignment_ids
    set created_assignment_ids [db_list assignment_ids "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp 
        where fp.freelance_package_id = fa.freelance_package_id
        and fp.project_id = :project_id
        and fa.assignment_status_id = 4220"]

    foreach created_assignment_id $created_assignment_ids {
	im_freelance_trans_send_request -assignment_id $created_assignment_id
    }

}
