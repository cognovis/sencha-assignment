# /packages/sencha-assignment/tcl/sencha-assignment.tcl
#

ad_library {
    Common procedures (helpers) for sencha-assignment package (mainly it is freelacner select screen)
    @author michaldrn@wp.pl
}



ad_proc -public im_freelance_calculate_deadline {
    { -assignment_id ""}
    { -freelance_package_id ""}
    { -update_deadline_p 0}
    { -multiple_freelance_package_ids ""}
    { -specific_package_type ""}
} {
    Proc which recalculates deadine

    @param update_deadline_p Update deadlines of follow up trans_steps task in case the deadline has moved
} {

    set user_id [ad_conn user_id]
    
    if {$assignment_id ne "" || $freelance_package_id ne "" || $multiple_freelance_package_ids ne ""} {
	
        if {$freelance_package_id eq "" && $multiple_freelance_package_ids eq ""} {
            db_0or1row assignment "select freelance_package_id from im_freelance_assignments fa where assignment_id =:assignment_id"
        }

        if {$multiple_freelance_package_ids eq ""} {
	    set multiple_freelance_package_ids $freelance_package_id
	}
	
	if {$multiple_freelance_package_ids eq ""} {
	    
	    return ""
	    
	} else {
	    
	    if {$specific_package_type eq ""} {
		db_1row package_type_id "select package_type_id as package_type_id from im_freelance_packages where freelance_package_id in([ns_dbquotelist $multiple_freelance_package_ids]) limit 1"
		set package_type_name [string tolower [im_category_from_id -translate_p 0 $package_type_id]]
	    } else {
		set package_type_name $specific_package_type
	    }
	    
	    set deadline_days 0
	    
	    db_1row project_info "select start_date as deadline from im_projects 
              where project_id = (select distinct project_id from im_freelance_packages 
              where freelance_package_id in ([ns_dbquotelist $multiple_freelance_package_ids]) limit 1)"
	    
            # Get the Previous Packages and sort them into the steps
            set steps_and_packages [im_freelance_assignments_step_packages -freelance_package_ids $multiple_freelance_package_ids]
	    
            # Generate an array of freelance_packages for each of the steps across all packages for which to calculate the deadline
            set steps [list]
	    
	   
	   foreach step_list $steps_and_packages {
	    
	       set step [lindex $step_list 0]
	       set freelance_package_ids($step) [lindex $step_list 1]
	       lappend steps $step
	       
	       # Find out if we have already assignments for the packages with deadlines
	       if {[exists_and_not_null freelance_package_ids($step)]} {
		   set assignments_end_date [db_string other_assignments "select max(end_date) from im_freelance_assignments where freelance_package_id in ([ns_dbquotelist $freelance_package_ids($step)]) and assignment_status_id not in (4223,4228,4230)" -default ""]
	       } else {
                    set assignments_end_date ""
	       }
               
	       # If we have an assignments end_date use that one, otherwise use the deadline
	       if {$assignments_end_date ne ""} {
		   set deadline "$assignments_end_date"
	       } else {
		   # Calculate the deadline from the steps
		   set assignable_task_ids [db_list assignable_task_ids "select distinct trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id in ([ns_dbquotelist $freelance_package_ids($step)])"]
		   set step_days [im_translation_task_processing_time -task_type $step -task_ids $assignable_task_ids]
		   set deadline "[im_translation_processing_deadline -start_timestamp $deadline -days $step_days]"
	       }
               set step_deadline($step) $deadline
	   }

	   # Overwrite the deadline if needed
	   if {$update_deadline_p eq 1 && $assignment_id ne ""} {
	       
	       # Find the step of the current assignment
	       set assignment_step [db_string current_assignment "select im_name_from_id(assignment_type_id) from im_freelance_assignments where assignment_id = :assignment_id" -default ""]
	       
	       if {$assignment_step ne ""} {
		   
		   set position [lsearch $steps $assignment_step]
		   set start_date $step_deadline($assignment_step)
		   set remaining_steps [lrange $steps [expr $position +1] end]
		   
		   # Update the deadline of any assignment in these packages if the deadline is later than the one already in the assignment
		   foreach remaining_step $remaining_steps {
		       
		       set step_type_id [db_string step_id "select category_id from im_categories where category_type = 'Intranet Trans Task Type' and category = :remaining_step"]
		       set remaining_assignments [db_list assignments "select assignment_id 
			                      from im_freelance_assignments
                            where freelance_package_id in ([ns_dbquotelist $freelance_package_ids($remaining_step)]) 
			                      and assignment_status_id not in (4223,4228,4230)
			                      and assignment_type_id = :step_type_id"]
		       
		       if {$remaining_assignments ne ""} {
			   
			   # Update the dates if they have been pushed back (later delivery)
			   db_dml update_assignments_start_date "update im_freelance_assignments set start_date = :start_date where assignment_id in ([ns_dbquotelist $remaining_assignments]) and start_date < :start_date"
			   
			   # Now that start_date is upadted, we also need to recalculate deadline
			   set assignable_task_ids [db_list assignable_task_ids "select distinct trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id in ([ns_dbquotelist $freelance_package_ids($step)])"]
			   set step_days [im_translation_task_processing_time -task_type $step -task_ids $assignable_task_ids]
			   set end_date "[im_translation_processing_deadline -start_timestamp $start_date -days $step_days]"
			   
			   db_dml update_assignments_start_date "update im_freelance_assignments set end_date = :end_date where assignment_id in ([ns_dbquotelist $remaining_assignments]) and end_date < :end_date"
			   # set the start date to the end date of the previous step
			   set start_date $end_date
			   
		       }			
		   }
	       }
	       
	   }	
	   return $step_deadline($package_type_name)
       }
    
    } else {
        ns_log Error " im_freelance_calculate_deadline: You must provide valid assignment_id or freelance_package_id for assignment_id $assignment_id
            -freelance_package_id $freelance_package_id
            -multiple_freelance_package_ids $multiple_freelance_package_ids"
    }

}


ad_proc -public im_get_language_parent {

{ -language_id 0}

} {
     Getting language parent @param language_id 
} {
   set language_parent_id 0
   if {$language_id ne 0} {
       set sql "select parent_id as language_parent_id from im_category_hierarchy ch where ch.child_id =:language_id limit 1"
       db_0or1row language_parent $sql
   }
   return $language_parent_id
}


ad_proc -public im_get_language_siblings {

{ -language_id 0}
{ -include_language_itself_p 0}
{ -include_parent_language_p 0}

} {
     Getting language siblings and (optionaly) parent by @param language_id

} {
   set language_siblings [list]
   # we need to make copy to later use it in case 'include_parent_language_p' is used
   set original_language_id $language_id
   if {$language_id ne 0} {
       set language_parent_id [im_get_language_parent -language_id $language_id]
       if {$language_parent_id ne 0} {
           set sql "select child_id from im_category_hierarchy ch where ch.parent_id =:language_parent_id"
           set language_siblings [db_list language_siblings $sql]
           if {$include_language_itself_p} { 
               lappend language_siblings $original_language_id
           }
           if {$include_parent_language_p} {
               lappend language_siblings $language_parent_id
           }
       }
   }
   return $language_siblings
}



ad_proc -public im_freelance_assignments_update_target_language_id {
} {
    Updating all assignments with target_language_id
} {

    set update_target_language_id_sql  "select freelance_package_id, freelance_package_name from im_freelance_packages"
    db_foreach update_target_language $update_target_language_id_sql {
        

        set single_task_target_language [db_0or1row single_task_target_language "select tt.target_language_id as package_language_id
                        from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt, im_freelance_packages fp
                        where tt.task_id = fptt.trans_task_id
                        and fp.freelance_package_id = fptt.freelance_package_id
                        and fp.freelance_package_id =:freelance_package_id
                        limit 1
                      
            "]
            if {$single_task_target_language ne 0} {
                db_dml update_assignment "update im_freelance_packages set target_language_id =:package_language_id where freelance_package_id =:freelance_package_id"
                ds_comment "package $freelance_package_name (id: $freelance_package_id) was updated to [im_category_from_id $package_language_id]"
            }
    }


}


ad_proc -public im_freelance_assignments_step_packages {
    -freelance_package_ids
} {
    Retrieve all the steps and packages for the tasks within this freelance_package
    
    @param freelance_package_id Package_id we want to get the packages for

    @return List of previous task steps and the packages. Return Empty if more than one project type is involved with different task_steps
} {
   
    set task_ids [db_list task_ids "select trans_task_id from im_freelance_packages_trans_tasks 
        where freelance_package_id in ([ns_dbquotelist $freelance_package_ids])"]

    set return_list [list]

    # Find out if we have different steps for the tasks based on the project_type 
    # Hope that the lower task_type_ids have the "normal" steps for this project
    # As the other ones will just be appended
    set task_project_type_ids [db_list task_type_ids "select distinct task_type_id from im_trans_tasks where task_id in ([ns_dbquotelist $task_ids]) order by task_type_id"]
    set steps [list]

    foreach task_type_id $task_project_type_ids {
        set task_types [split [db_string task_type "select aux_string1 from im_categories where category_id = :task_type_id" -default ""]]
        foreach task_type $task_types {
            if {[lsearch $steps $task_type]<0} {
                 lappend steps $task_type
             }
         }
    }
    
    # Get the packages for each step
    foreach step $steps {
        set task_type_id [db_string task_type "select category_id from im_categories where category_type = 'Intranet Trans Task Type' and (lower(category) = :step or lower(category) = initcap(:step))" -default 0]
            if {$task_type_id ne 0} {
                set package_ids [db_list package_ids "select fp.freelance_package_id from im_freelance_packages_trans_tasks fptt, im_freelance_packages fp
                    where trans_task_id in ([ns_dbquotelist $task_ids]) 
                    and package_type_id = :task_type_id
                    and fptt.freelance_package_id = fp.freelance_package_id"]
                    lappend return_list [list $step $package_ids]
            }
    }
    
    return $return_list
}
ad_proc -public im_freelancer_project_langs {
    -freelancer_id
    -project_id
    {-type "target"}
} {
    Return the list of languages a freelancer can translate in the project - memoized
    
    @param freelancer_id ID of the freelancer
    @param project_id ID of the project for which we look up the freelancer languages
    @param type used to deferentiate between source and target languages
	
    @return List of target language_ids the freelancer can translate into. Specific to this project
} {

    set project_lang_ids [list]

    if {$type eq "target"} {

	# Get the list of possible target languages in the project
 	set language_sql "select  language_id as target_language_id, parent_id
		from    im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
		where   project_id = :project_id"
	
	db_foreach language $language_sql {
	    if {[exists_and_not_null parent_id]} {
		set project_lang_ids [concat $project_lang_ids [im_sub_categories $parent_id]]
	    } else {
		set project_lang_ids [concat $project_lang_ids [im_sub_categories $target_language_id]]
	    }
	}
	
	set skill_type_id 2002

    } else {
	set skill_type_id 2000

 	db_1row source_language "select source_language_id, ch.parent_id
		from    im_projects left outer join im_category_hierarchy ch on (child_id = source_language_id)
		where   project_id = :project_id"
	
	if {[exists_and_not_null parent_id]} {
	    set project_lang_ids [im_sub_categories $parent_id]
	} else {
	    set project_lang_ids [im_sub_categories $source_language_id]
	}
    }

    set freelancer_lang_ids [db_list fl_languages "select distinct skill_id from im_freelance_skills 
        where skill_type_id=:skill_type_id 
        and skill_id in ([ns_dbquotelist $project_lang_ids])
        and user_id = :freelancer_id"] 

    set freelancer_lang_ids [lsort -unique $freelancer_lang_ids]

    # Append the parent lang_ids
    if {[llength $freelancer_lang_ids]>0} {
        set parent_ids [db_list freelancer_parent_langs "select distinct parent_id from im_category_hierarchy where child_id in ([ns_dbquotelist $freelancer_lang_ids])"]
        set freelancer_lang_ids [concat $parent_ids $freelancer_lang_ids]
    }
    
    return [lsort -unique $freelancer_lang_ids]
}


ad_proc -public im_assignment_freelancer_ratings {
    -freelancer_id
    {-json ""}
} {


} {

    set freelancer_quality_list [list]
    db_foreach freelancer_quality "select quality_level_id, category_name as quality_level_name from im_categories c, im_assignment_quality_reports fq, acs_objects acso 
        where acso.object_id = fq.report_id
        and im_categories c.category_id = fq.quality_level_id
        and acso.context_id =651566
        group by quality_level_id" {
    lappend freelancer_quality_list [list $quality_level_id]
  }
  return $freelancer_quality_list

}

ad_proc -public sencha_project_member_component {
  -project_id
  -current_user_id
} {
  Procedure to return the project_members
} {

  set name_order [parameter::get -package_id [apm_package_id_from_key intranet-core] -parameter "NameOrder" -default 1]
  set group_l10n [lang::message::lookup "" intranet-core.Group "Group"]
  set object_id $project_id

  set return_url [export_vars -base "/sencha-assignment/project-manager-app" -url {project_id}]
  set sql_query "
    select
      rels.object_id_two as user_id, 
      rels.object_id_two as party_id, 
      im_email_from_user_id(rels.object_id_two) as email,
      coalesce(
        im_name_from_user_id(rels.object_id_two, $name_order), 
        :group_l10n || ': ' || acs_object__name(rels.object_id_two)
      ) as name,
      im_category_from_id(c.category_id) as member_role,
      c.category_gif as role_gif,
      c.category_description as role_description
    from
      acs_rels rels
      LEFT OUTER JOIN im_biz_object_members bo_rels ON (rels.rel_id = bo_rels.rel_id)
      LEFT OUTER JOIN im_categories c ON (c.category_id = bo_rels.object_role_id)
    where
      rels.object_id_one = :project_id and
      rels.object_id_two in (select party_id from parties) and
      rels.object_id_two not in (
        -- Exclude banned or deleted users
        select  m.member_id
        from  group_member_map m,
          membership_rels mr
        where m.rel_id = mr.rel_id and
          m.group_id = acs__magic_object_id('registered_users') and
          m.container_id = m.group_id and
          mr.member_state != 'approved'
      )
    order by 
      name  
  "

  set add_admin_links 0
  set project_lead_id [db_string project_lead "select project_lead_id from im_projects where project_id = :project_id" -default ""]

  if {$current_user_id eq $project_lead_id} {
    set add_admin_links 1
  }

  # ------------------ Format the table header ------------------------
  set colspan 1
  set header_html "
      <tr> 
  <td class=rowtitle align=middle>[_ intranet-core.Name]</td>
  "
  

  if {$add_admin_links} {
    incr colspan
    append header_html "<td class=rowtitle align=middle><input type='checkbox' name='_dummy' onclick=\"acs_ListCheckAll('delete_user',this.checked)\"></td>"
  }
  append header_html "
  </tr>"

  # ------------------ Format the table body ----------------
  set td_class(0) "class=roweven"
  set td_class(1) "class=rowodd"
  set found 0
  set count 0
  set body_html ""
  set output_hidden_vars ""
  db_foreach users_in_group $sql_query {

    # Make up a GIF with ALT text to explain the role (Member, Key 
    # Account, ...
    set descr $role_description
    if {"" == $descr} { set descr $member_role }

    # Allow for object type specific localization of GIF and comment
    set member_role_key [lang::util::suggest_key $member_role]
    set descr_otype_key "intranet-core.Role_im_project_$member_role_key"
    set descr [lang::message::lookup "" $descr_otype_key $descr]
    set role_gif_key "intranet-core.Role_GIF_[lang::util::suggest_key $role_gif]"
    set role_gif [lang::message::lookup "" $role_gif_key $role_gif]
  
    set profile_gif [im_gif -translate_p 0 $role_gif $descr]

    incr count
    if { $current_user_id == $user_id } { set found 1 }

    # determine how to show the user: 
    # -1: Show name only, 0: don't show, 1:Show link
    set show_user [im_show_user_style $user_id $current_user_id $project_id]
    if {$show_user == 0} { continue }

    append output_hidden_vars "<input type=hidden name=member_id value=$user_id>"
    append body_html "
      <tr $td_class([expr $count % 2])>
        <td>
    "

    if {$show_user > 0} {
      append body_html "<A HREF=/intranet/users/view?user_id=$user_id>$name</A>"
    } else {
      append body_html $name
    }

    append body_html "$profile_gif</td>"
  
    if {$add_admin_links} {
      append body_html "
        <td align=middle>
          <input type='checkbox' name='delete_user' id='delete_user,$user_id' value='$user_id'>
 
        </td>
      "
    }
    append body_html "</tr>"
  }

  if { [empty_string_p $body_html] } {
    set body_html "<tr><td colspan=$colspan><i>[_ intranet-core.none]</i></td></tr>\n"
  }

  # ------------------ Format the table footer with buttons ------------
  set footer_html ""
  if {$add_admin_links} {
    append footer_html "
      <tr>
        <td align=left>
    <ul>
    <li><A HREF=\"[export_vars -base "/intranet/member-add" -url {object_id return_url}]\">[_ intranet-core.Add_member]</A>
    </ul>
        </td>
    "

    append footer_html "
      <tr>
        <td align=right colspan=$colspan>
    <select name=action>
    "

    append footer_html "
      <option value=del_members>[_ intranet-core.Delete_members]</option>
      </select>
      <input type=submit value='[_ intranet-core.Apply]' name=submit_apply></td>
        </td>
      </tr>
    "
  }

  # ------------------ Join table header, body and footer ----------------
  set html "
    <form method=POST action=/intranet/member-update>
    $output_hidden_vars
    [export_form_vars object_id return_url]
      <table bgcolor=white cellpadding=1 cellspacing=1 border=0>
        $header_html
        $body_html
        $footer_html
      </table>
  </form>
    "
  return $html
}