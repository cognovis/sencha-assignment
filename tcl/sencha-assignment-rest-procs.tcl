# /packages/sencha-freelance-translation/tcl/sencha-freelance-translation-procs.tcl
#
# Copyright (c) 2003-2006 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_library {
    Common procedures to implement translation freelancer functions:
    @author frank.bergmann@project-open.com
}


ad_proc -public im_rest_get_custom_sencha_project_assignments {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on project assignments
} {
    
    array set query_hash $query_hash_pairs

    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    } 

    if {[info exists query_hash(assignee_id)]} {
        set assignee_id $query_hash(assignee_id)
    } 
    
    db_1row project_info "select aux_string1, project_nr, project_name, project_lead_id, im_name_from_id(project_lead_id) as project_lead, end_date from im_projects, im_categories where project_id = :project_id and project_type_id = category_id"
    #set project_id 648320
    #set assignee_id 99092
    
    set assignments_sql "select distinct freelance_package_name, assignment_id, coalesce(assignment_units,0) as assignment_units, uom_id, im_name_from_id(uom_id) as uom, rate, assignment_status_id, im_name_from_id(assignee_id) as assignee_name,
        im_name_from_id(assignment_type_id) as assignment_type, start_date as start_date, end_date as end_date,
        purchase_order_id, fp.freelance_package_id, assignment_type_id, im_name_from_id(assignment_type_id) as assignment_type_name, assignment_status_id, im_name_from_id(assignment_status_id) as assignment_status_name
    from im_freelance_assignments fa, im_freelance_packages fp
    where project_id = :project_id and fp.freelance_package_id = fa.freelance_package_id and assignee_id = :assignee_id
    and fa.assignment_status_id <> 4230"

    set assigments ""
    set obj_ctr 0

    db_foreach assigment $assignments_sql {

        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        set start_date [lc_time_fmt $start_date "%q %X"]
        set deadline [lc_time_fmt $end_date "%q %X"]

        set single_assigment "{\"assignment_id\": $assignment_id,\"assignment_type_id\": $assignment_type_id,\"assignment_type_name\": \"$assignment_type_name\",\"assignment_status_id\": $assignment_status_id,\"assignment_status_name\": \"$assignment_status_name\",\n\"assignee_name\":\"$assignee_name\",\n\"package_id\": \"$freelance_package_id\",\n\"package_name\": \"$freelance_package_name\",\n\"package_name\": \"$freelance_package_name\", \"unit_of_measure\":\"$uom\", \"units\":\"$assignment_units\", \"start_date\":\"$start_date\", \"end_date\":\"$deadline\"}"
        append assigments $single_assigment
        incr obj_ctr
    }
    
    set total $obj_ctr
    set project_info "{\"project_attribute_name\":\"$project_name\"}"
    #set data "{\"project_assignments\":\[\n$assigments\n\]\n, \"project_info\":\[\n$project_info\n\]\n}"
     
    #set result "{\"success\": true,\n\"total\": $total,\n\"data\": $data}"
    set result "{\"success\": true,\n\"total\": $total,\n\"data\": \[\n$assigments\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}

ad_proc -public im_rest_get_custom_sencha_project_assignments_info {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on project information
} {
    
    array set query_hash $query_hash_pairs

    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    } 
    
    if {[info exists query_hash(assignee_id)]} {
        set assignee_id $query_hash(assignee_id)
    } 

    set project_info ""
    set obj_ctr 0
     
    db_1row project_info "select aux_string1, project_nr, project_name, project_lead_id, im_name_from_id(subject_area_id) as subject_area_name, im_name_from_id(project_lead_id) as project_lead, end_date from im_projects, im_categories where project_id = :project_id and project_type_id = category_id"
    
    db_1row assigment_info "select im_name_from_id(assignee_id) as assignee_name, im_name_from_id(source_language_id) as source_language, im_name_from_id(target_language_id) as target_language from im_freelance_assignments fa, im_freelance_packages fp, im_materials m where m.material_id = fa.material_id and fp.freelance_package_id = fa.freelance_package_id and fp.project_id =:project_id and fa.assignee_id = :assignee_id"
    set translation_direction "$source_language => $target_language"

    append project_info "{\"project_attribute_name\":\"Language\", \"project_attribute_value\":\"$translation_direction\"},"
    append project_info "{\"project_attribute_name\":\"Project Nr\", \"project_attribute_value\":\"$project_nr\"}," 
    append project_info "{\"project_attribute_name\":\"Project Name\", \"project_attribute_value\":\"$project_name\"}," 
    append project_info "{\"project_attribute_name\":\"Project Manager\", \"project_attribute_value\":\"$project_lead\"}," 
    append project_info "{\"project_attribute_name\":\"Assignee\", \"project_attribute_value\":\"$assignee_name\"},"
    #append project_info "{\"project_attribute_name\":\"Honorar Gesamt\", \"project_attribute_value\":\"0.00 €\"},"
    append project_info "{\"project_attribute_name\":\"Subject Area\", \"project_attribute_value\":\"$subject_area_name\"},"
    #append project_info "{\"project_attribute_name\":\"Business Area\", \"project_attribute_value\":\"$subject_area_name\"}" 

    set result "{\"success\": true,\n\"data\": \[\n$project_info\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}


ad_proc -public im_rest_get_custom_sencha_existing_project_new_task {
    -project_id
    -target_language_ids
    { -task_id 0 }
    { -task_name "" }
    { -task_units 0 }
    { -task_uom_id 0 }
    { -task_type_id "" }
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -deadline "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
     Handler for GET rest call which creates new task for existing project

     @param project_id ID of the project for which we want to create new task
     @param task_id ID of the task, this indicates that we are actually not creating new task, but editing existing one
     @param task_name Name of new task
     @param task_uom_id New task unit of measure id
     @param task_units New task number of units
     @param task_type_id New task type id. Should take value of project_type_id as default
     @param tadget_language_ids List of ids
     @param deadline deadline for thr task

     @return result result of new task creation
     @return error potential error
} {
   
    set task_insert_error ""
    set task_filename ""

    if {$task_id eq 0} {

        #ns_log Notice "task-action: Add manual task: im_task_insert $project_id [ns_urldecode $task_name_manual] $task_filename $task_units_manual $task_uom_manual $task_type_manual $target_language_ids"
    
        if {$target_language_ids eq "all"|| $target_language_ids == 0} {
            # getting all possible project target languages ids
            set target_language_ids [im_target_language_ids $project_id]
        }

        #set project_type_id [db_string project_type_id "select project_type_id from im_projects where project_id =:project_id" -default 0]
        set project_type_id $task_type_id

        set new_task_id [im_task_insert $project_id $task_name $task_filename $task_units $task_uom_id $project_type_id $target_language_ids]
        if {$new_task_id ne 0} {
            set result true
        } else {
            set result false
        }

        im_translation_update_project_dates -project_id $project_id

    } else {
        # Check if task exist
        set task_exist_p [db_string task_exist_p_sql "select 1 from im_trans_tasks where task_id =:task_id" -default 0]
        if {$task_exist_p eq 1} {
            if {true} {
                set end_date_sql ""
            } else {
                set splitted_deadline [split  $deadline "_"]
                set deadline_date [lindex $splitted_deadline 0]
                set deadline_hour [lindex $splitted_deadline 1]
                set parsed_deadline "$deadline_date $deadline_hour"
                set end_date_sql ", end_date =:parsed_deadline"
            }
            
            set task_update_sql "update im_trans_tasks set task_name =:task_name, task_filename = :task_filename, task_units = :task_units, task_uom_id =:task_uom_id, task_type_id =:task_type_id $end_date_sql where task_id =:task_id"
            db_dml task_update $task_update_sql
            set result true
        }

    }

    set result "{\"success\": $result, \"error\":\"[im_quotejson $task_insert_error]\"}"
    
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_existing_project_tasks {
    -project_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on project freelancer trans tasks (from im_trans_tasks)
     
    @param project_id ID of the project for which we get the existing tasks
     
    @return object_id ID of the task
    @return task_id Same as object_id
    @return task_name Name of the task
    @return task_units Units for the task
    @return unit_of_measure ID of the unit of measure
    @return unit_of_measure_name I18N Name of the measure
    @return target_language_id Language into which this task is translated
    @return target_language_name I18N name of the target language
    @return task_type_id id of task type
    @return task_type_name name of task type
    @return task_deadline Deadline of the task
} {


    set existing_project_tasks_sql "select * from im_trans_tasks where project_id =:project_id and task_status_id <> 372"


    
    set existing_project_tasks [list]

    db_1row project_type_id "select project_type_id from im_projects where project_id =:project_id"

    db_foreach existing_tasks $existing_project_tasks_sql {

        set object_id $task_id
        set unit_of_measure $task_uom_id
        set unit_of_measure_name [im_category_from_id $task_uom_id]
        set task_deadline ""
        set task_type_name [im_category_from_id $task_type_id]
        set target_language_name [im_category_from_id $target_language_id]
        set task_packages [db_list task_packags "select freelance_package_id from im_freelance_packages_trans_tasks where trans_task_id =:task_id"]
        set previous_assignment_status_id 0

    # This needs to be ammended to support the trans steps
    # Otherwise we won't be able to correctly display the deadline for copy + edit process.

        foreach freelance_package_id $task_packages {
            set existing_assignment_deadline [db_0or1row existing_assignment_deadline "select end_date, assignment_status_id from im_freelance_assignments where freelance_package_id =:freelance_package_id and assignment_status_id in (4225,4226)"]
            if {$end_date ne ""} {
                
        set task_deadline $end_date
            }
            if {[exists_and_not_null assignment_status_id]} {
                set previous_assignment_status_id $assignment_status_id
            }
        }

        if {$task_deadline eq ""} {
            # Getting steps
            if {[llength $task_packages] > 0} {
                set steps [im_freelance_assignments_step_packages -freelance_package_ids $task_packages]
            } else {
                set steps [db_string project_type_steps "select aux_string1 from im_categories where category_id =:project_type_id"]
            }
            set first_step_name [lindex [lindex $steps 0] 0]
            set task_deadline [db_string task_deadline "select ${first_step_name}_end_date as task_deadline from im_trans_tasks where task_id =:task_id" -default ""]
        }
        lappend existing_project_tasks [im_rest_json_object -proc_name im_rest_get_custom_sencha_existing_project_tasks]
    }
    
    set objects_result [join $existing_project_tasks ",\n"]
    set result "{\"success\": true,\n\"total\": [llength $existing_project_tasks],\n\"data\": \[\n$objects_result\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return
}



# ---------------------------------------------------------------
# Select Freelancers to projects
# ---------------------------------------------------------------

ad_proc -public im_rest_get_custom_freelancers_for_assignment {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for getting information on Translation freelancers

    Requires the project_id

    Returns a JSON with
    - Freelancer ID and name
    - Source & Target languages
    - Skills (to be defined which ones)
    - Rates (source word & hours)

    See /sencha-freelance-translation/lib/freelance-trans-member.tcl for details
} {
    set result ""
    array set query_hash $query_hash_pairs
    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    } else {
        # We can't work without a project!
        im_rest_error -format "json" -http_status 403 -message "Missing a project_id"
    }
    
    # Anyone who can write on this project can query the freelancers for it
    im_project_permissions $rest_user_id $project_id view_p read_p write_p admin_p
    if {!$write_p} {return}

    set valid_vars [list freelancer_name no_times_worked_for_customer no_times_trans_for_customer no_times_edit_for_customer price_s_word price_hour selected_p potential_p average_rating active_project_member_p matched_target_lang_id sort_order source_language_exp_level target_language_exp_level matched_lang_exp_level existing_assignment_status_id existing_assignment_types_ids existing_assignment_types_names matched_lang_exp_level_id main_trans_p]

    # Get the skill information for the skills to return
    set skill_type_sql ""

    db_foreach skill_types {select skill_type_id, object_type_id, display_mode
        from im_freelance_skill_type_map, im_projects
        where object_type_id = project_type_id and project_id = :project_id and display_mode != 'none'
    } {
            append skill_type_sql "\t\t(select '\[' || array_to_string(array_agg(skill_id), ',') || '\]' from im_freelance_skills where skill_type_id = $skill_type_id and user_id = m.member_id) as skill_$skill_type_id,\n"
#           append skill_type_sql "\t\tim_freelance_skill_list(m.member_id, $skill_type_id) as skill_$skill_type_id,\n"
            lappend valid_vars "skill_$skill_type_id"
    }

    #set target_language_exp_level "dummy"
    
    # Source & Target
    foreach skill_type_id [list 2000 2002 10000432] {
        if {[lsearch $valid_vars skill_$skill_type_id]<0} {
                append skill_type_sql "\t\t(select '\[' || array_to_string(array_agg(skill_id), ',') || '\]' from im_freelance_skills where skill_type_id = $skill_type_id and user_id = m.member_id) as skill_$skill_type_id,\n"
                lappend valid_vars "skill_$skill_type_id"
        }
    }

    # ---------------------------------------------------------------
    # Languages
    # ---------------------------------------------------------------

    # Project's Source & Target Languages
    if {[db_0or1row source_langs "select  source_language_id, ch.parent_id
                from    im_projects left outer join im_category_hierarchy ch on (child_id = source_language_id)
                where   project_id = :project_id"]} {

    set source_languages [list]

    if {[exists_and_not_null parent_id]} {
        set source_language_ids [im_sub_categories $parent_id]
    } else {
        set source_language_ids [im_sub_categories $source_language_id]
    }
    } else {
    # Seems to be missing something
    set source_language_ids ""
    }

    set target_language_ids [list]
    db_foreach target_langs "
            select  language_id as target_language_id, parent_id
            from    im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
            where   project_id = :project_id
    " {
        if {[exists_and_not_null parent_id]} {
            set target_ids [im_sub_categories $parent_id]
        } else {
            set target_ids [im_sub_categories $target_language_id]
        }
        foreach target_id $target_ids {
            if {[lsearch $target_language_ids $target_id]<0} {
                lappend target_language_ids $target_id
            }
        }
    }

    if {$source_language_ids ne ""} {
        set source_lang_from ",(select user_id from im_freelance_skills where skill_type_id = 2000 and skill_id in ([ns_dbquotelist $source_language_ids])) source_lang"
        set source_lang_where "AND m.member_id = source_lang.user_id"
    } else {
        set source_lang_from ""
        set source_lang_where ""
        set source_lang ""
    }


    # Override if we have the skill_2002
    if {[info exists query_hash(skill_2002)]} {
        set target_language_ids $query_hash(skill_2002)
    }
    if {$target_language_ids ne ""} {
        set target_lang_from ",(select user_id from im_freelance_skills where skill_type_id = 2002 and skill_id in ([ns_dbquotelist $target_language_ids])) target_lang"
        set target_lang_where " AND m.member_id = target_lang.user_id"
    } else {
        set target_lang_from ""
        set target_lang_where ""
        set target_lang ""
    }

    set customer_id [db_string company "select coalesce(final_company_id,company_id) as company_id from im_projects where project_id = :project_id" -default "0"]
    set company_id [db_string company_id "select company_id from im_projects where project_id =:project_id" -default 0]

    set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]

    # Get the project_type to find out the order of trans steps

    set trans_steps [split [db_string get_steps "select aux_string1 from im_categories c, im_projects p where p.project_id = :project_id and p.project_type_id = c.category_id" -default ""] " "]
    
    set project_type_id [db_string project_type_id "select project_type_id from im_projects where project_id =:project_id" -default 0]
    
    # Get the task_type_ids for the project
    set project_task_types [db_string task_types "select aux_string1 from im_categories c, im_projects p where p.project_type_id = c.category_id and project_id = :project_id" -default ""]
    set task_type_ids [list]
    foreach task_type [split $project_task_types " "] {
	    lappend task_type_ids [db_string task_type_id "select category_id from im_categories where lower(aux_string1) = lower(:task_type) and category_type = 'Intranet Project Type' limit 1" -default ""]
    }

    set freelance_sql "
        select distinct
            m.member_id as user_id
        from
            group_member_map m,
            (select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([ns_dbquotelist $company_status_ids])) f
            $source_lang_from
            $target_lang_from
        where m.group_id = [im_profile_freelancers] and m.member_id not in (
                -- Exclude banned or deleted users
                select  m.member_id
                from    group_member_map m,
                    membership_rels mr
                where   m.rel_id = mr.rel_id and
                    m.group_id = acs__magic_object_id('registered_users') and
                    m.container_id = m.group_id and
                    mr.member_state != 'approved') AND
            f.object_id_two = m.member_id
            $source_lang_where
            $target_lang_where
        "
            
    # ---------------------------------------------------------------
    # Build the price array for freelancers
    # ---------------------------------------------------------------

    set where_clauses [list]

    set task_type_ids [list]
    # Source language filter
    if {[llength $source_language_ids] >0} {
	lappend where_clauses "(p.source_language_id in ([ns_dbquotelist $source_language_ids]) or p.source_language_id is null)"
    }
    if {[llength $target_language_ids] >0} {
	lappend where_clauses "(p.target_language_id in ([ns_dbquotelist $target_language_ids]) or p.target_language_id is null)"
    }
    if {[llength $task_type_ids]>0} {
	lappend where_clauses "p.task_type_id in ([ns_dbquotelist $task_type_ids])"
    }

    if {[llength $where_clauses]<1} {
	set where_clauses "1=1"
    }

    set price_sql "
        select
            f.user_id,
            p.uom_id,
            im_category_from_id(p.task_type_id) as task_type,
            im_category_from_id(p.source_language_id) as source_language,
            im_category_from_id(p.target_language_id) as target_language,
            im_category_from_id(p.subject_area_id) as subject_area,
            im_category_from_id(p.file_type_id) as file_type,
            min(p.price) as low_price,
            max(p.price) as high_price,
            p.currency,
            p.min_price
        from
            ($freelance_sql) f
            LEFT OUTER JOIN acs_rels uc_rel ON (f.user_id = uc_rel.object_id_two)
            LEFT OUTER JOIN im_trans_prices p ON (uc_rel.object_id_one = p.company_id)
        where
            [join $where_clauses " and "]
        group by
            f.user_id,
            p.uom_id,
            p.task_type_id,
            p.source_language_id,
            p.target_language_id,
            p.subject_area_id,
            p.file_type_id,
            p.currency,
            p.min_price
            order by task_type"

    db_foreach price_hash_sql $price_sql {
        if {$uom_id ne ""} {
            set key "$user_id-$uom_id"

            # Calculate the base cell value
            set price_append "$low_price-$high_price"
            if {$low_price == $high_price} { set price_append "$low_price" }
            # Add the list of parameters
	    # Should only be price and task type
	    
	    if {$min_price ne ""} {
		set price_append "$price_append\(>$min_price\)"
	    }
            set param_list [list $price_append "$source_language=>$target_language"]
	    # Optionally we can add more elements later
            if {"" == $source_language && "" == $target_language} { set param_list [list $price_append] }
            if {"" != $subject_area} { lappend param_list $subject_area }
            if {"" != $task_type} { lappend param_list $task_type }
            # if {"" != $file_type} { lappend param_list $file_type }
            # if {"" != $currency} { lappend param_list $currency }
            # set hash [im_freelance_trans_member_select_component_join_prices $hash $param_list]
            # Update the hash table cell
            if {[info exists price_hash($key)]} {
		if {[lsearch $price_hash($key) $param_list]<0} {
		    lappend price_hash($key) $param_list
		}
	    } else {
		set price_hash($key) [list $param_list]
	    }

        }
    }

    # Transform into JSON format
    foreach key [array names price_hash] {
        set values $price_hash($key)
        set result_list [list]
        foreach value $values {
            lappend result_list "\"[lindex $value 0] {[lrange $value 1 end]}\""
        }
        set price_hash($key) "\[[join $result_list ,]\]"
    }

    # ---------------------------------------------------------------
    # Generate the JSON for each freelancer
    # ---------------------------------------------------------------

    set obj_ctr 0
    
    # ---------------------------------------------------------------
    # Generate the from clauses for sorting
    # ---------------------------------------------------------------
    set from_sql ""

    # Get the confirmed assignees
    set confirmed_assignee_ids [db_list assignees "select assignee_id from im_freelance_assignments fa, im_freelance_packages fp 
         where fp.freelance_package_id = fa.freelance_package_id and fp.project_id = :project_id and assignment_status_id in (4222,4224,4225,4226,4227,4229)"]

    if {$confirmed_assignee_ids ne ""} {
    append from_sql "case when m.member_id in ([ns_dbquotelist $confirmed_assignee_ids]) then '1' else '0' end as confirmed_assignee_p,"
    } else {
    append from_sql "0 as confirmed_assignee_p,"
    }


    # Get the requested assignees
    set requested_assignee_ids [db_list assignees "select assignee_id from im_freelance_assignments fa, im_freelance_packages fp 
         where fp.freelance_package_id = fa.freelance_package_id and fp.project_id = :project_id and assignment_status_id in (4221,4220)"]

    if {$requested_assignee_ids ne ""} {
    append from_sql "case when m.member_id in ([ns_dbquotelist $requested_assignee_ids]) then '1' else '0' end as requested_assignee_p,"
    } else {
    append from_sql "0 as requested_assignee_p,"
    }
    

    # Get the project members
    set project_member_ids [im_biz_object_member_ids $project_id]


    # Assignees are also project members
    set project_member_ids [concat $project_member_ids $confirmed_assignee_ids $requested_assignee_ids] 

    if {$project_member_ids ne ""} {
    append from_sql "case when m.member_id in ([ns_dbquotelist $project_member_ids]) then '1' else '0' end as active_project_member_p,"
    } else {
    append from_sql "0 as active_project_member_p,"
    }
    
    # Get the main translators
    set sql "select distinct freelancer_id from im_trans_main_freelancer where customer_id = :company_id and source_language_id in ([ns_dbquotelist $source_language_ids]) and target_language_id in ([ns_dbquotelist $target_language_ids])"
    set main_translator_ids [db_list freelancer "$sql"]


    if {$main_translator_ids ne ""} {
    append from_sql "case when m.member_id in ([ns_dbquotelist $main_translator_ids]) then '1' else '0' end as main_trans_p,"
    } else {
    append from_sql "0 as main_trans_p,"
    }

    # Potential Members should be shown afterwards
    set potential_status_ids [im_sub_categories [im_company_status_potential]]

    set source_language_exp_level_sql "(select max(im_categories.aux_int1) AS experience from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = m.member_id AND im_freelance_skills.skill_id in ([ns_dbquotelist $source_language_ids]) AND im_freelance_skills.skill_type_id = 2000) as source_lang_experience,"
    
    #set add_language_exp_level_sql "(select im_categories.aux_int1 AS experience from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = m.member_id AND im_freelance_skills.skill_id in ([ns_dbquotelist $target_language_ids]) AND im_freelance_skills.skill_type_id = 2002) as target_experien,"

    set sort_order 0
    set max_quality_projects 0

    set freelancer_ids  [db_list freelancers "select distinct
                m.member_id as user_id
            from
                group_member_map m,
                (select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([ns_dbquotelist $company_status_ids])) f
                $source_lang_from
                $target_lang_from
            where m.group_id in ([im_profile_freelancers],[im_profile_employees]) and m.member_id not in (
                    -- Exclude banned or deleted users
                    select  m.member_id
                    from    group_member_map m,
                        membership_rels mr
                    where   m.rel_id = mr.rel_id and
                        m.group_id = acs__magic_object_id('registered_users') and
                        m.container_id = m.group_id and
                        mr.member_state != 'approved') AND
                f.object_id_two = m.member_id
                $source_lang_where
                $target_lang_where"]

    if {$freelancer_ids ne ""} {
    foreach task_type $trans_steps {
        db_foreach customers "select count(*) as worked_with, freelancer_id from 
                (select distinct tree_root_key(p.tree_sortkey), ${task_type}_id as freelancer_id
                    from im_trans_tasks t, im_projects p
                    where (p.company_id = :customer_id or p.final_company_id = :customer_id)
                    and p.project_id = t.project_id
                                        and ${task_type}_id in ([ns_dbquotelist $freelancer_ids])) t group by freelancer_id" {
                        set worked_with_${task_type}_customer($freelancer_id) $worked_with
                    }
        db_foreach worked_with "select count(distinct t.project_id) as worked_with, ${task_type}_id as freelancer_id from im_trans_tasks t where ${task_type}_id in ([ns_dbquotelist $freelancer_ids]) group by freelancer_id" {
        set worked_with_${task_type}($freelancer_id) $worked_with
        }
    }
    }
    
    set subject_area_id_of_project [db_string subject_area_id_of_project_sql "select subject_area_id from im_projects where project_id=:project_id" -default ""]
    set average_rating_sql "(select avg(aux_int1) from im_categories c, im_assignment_quality_report_ratings rr, im_assignment_quality_reports r, im_freelance_assignments fa where rr.report_id = r.report_id and c.category_id = rr.quality_level_id and r.assignment_id = fa.assignment_id and fa.assignee_id = m.member_id and r.subject_area_id=:subject_area_id_of_project) as average_rating,"

    set freelance_sql "
            select distinct
                m.member_id as user_id,
                $source_language_exp_level_sql
                $average_rating_sql
                im_name_from_user_id(m.member_id) as object_name,
                $skill_type_sql
                $from_sql
                case when company_status_id in ([ns_dbquotelist $potential_status_ids]) then '1' else '0' end as potential_p
            from
                group_member_map m,
                (select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([ns_dbquotelist $company_status_ids])) f
                $source_lang_from
                $target_lang_from
            where m.group_id in ([im_profile_freelancers],[im_profile_employees]) and m.member_id not in (
                    -- Exclude banned or deleted users
                    select  m.member_id
                    from    group_member_map m,
                        membership_rels mr
                    where   m.rel_id = mr.rel_id and
                        m.group_id = acs__magic_object_id('registered_users') and
                        m.container_id = m.group_id and
                        mr.member_state != 'approved') AND
                f.object_id_two = m.member_id
                $source_lang_where
                $target_lang_where
            order by confirmed_assignee_p desc, requested_assignee_p desc, active_project_member_p desc, main_trans_p desc, potential_p desc, average_rating desc, object_name desc
        " 

    db_foreach select_freelancers $freelance_sql {
        incr sort_order
        set freelancer_name $object_name
        set source_language_exp_level $source_lang_experience
	
        # Append the prices not for employees
        set uom_listlist "{324 price_s_word} {320 price_hour}"
        foreach uom_tuple $uom_listlist {
            set uom_id [lindex $uom_tuple 0]
            set var_name [lindex $uom_tuple 1]
            set key "${user_id}-${uom_id}"
            if { [info exists price_hash($key)] && ![im_user_is_employee_p $user_id]} {
                set $var_name $price_hash($key)
            } else {
		set $var_name ""
	    }
        }
        
        # ---------------------------------------------------------------
        # Find out the number worked with
        # The frontend assumes "trans + edit" as steps
        # We therefore have to ammend this to the real steps
        # Eventually have to change frontend :-)
        # ---------------------------------------------------------------
            if {[info exists worked_with_[lindex $trans_steps 0]_customer($user_id)]} {
            set no_times_trans_for_customer [set worked_with_[lindex $trans_steps 0]_customer($user_id)]
            } else {
            set no_times_trans_for_customer 0
            }
            if {[llength $trans_steps]>1 && [info exists worked_with_[lindex $trans_steps 1]_customer($user_id)]} {
            set no_times_edit_for_customer [set worked_with_[lindex $trans_steps 1]_customer($user_id)]
        } else {
            set no_times_edit_for_customer 0
        }

        set no_times_worked_for_customer [expr $no_times_trans_for_customer + $no_times_edit_for_customer]
            if {$no_times_worked_for_customer <2 && [info exists worked_with_[lindex $trans_steps 0]($user_id)]} {
            set no_times_trans_for_customer [set worked_with_[lindex $trans_steps 0]($user_id)]
            if {[llength $trans_steps]>1 && [info exists worked_with_[lindex $trans_steps 1]($user_id)]} {
                set no_times_edit_for_customer [set worked_with_[lindex $trans_steps 1]($user_id)]
            } else {
                set no_times_edit_for_customer 0
            }
        }
        
        # ---------------------------------------------------------------
        # Matched Target Language
        # ---------------------------------------------------------------
        set matched_target_lang_id [list]

        
        regsub -all {\[} $skill_2002 {} target_skill_ids
        regsub -all {\]} $target_skill_ids {} target_skill_ids
        foreach match_target_lang_id [split $target_skill_ids ","] {
            if {[lsearch $target_language_ids $match_target_lang_id]>-1} {
                lappend matched_target_lang_id $match_target_lang_id
            }
        }

        db_0or1row target_language_exp_level_sql "select im_categories.aux_int1 AS target_language_exp_level from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = :user_id AND im_freelance_skills.skill_id in ([ns_dbquotelist $match_target_lang_id]) AND im_freelance_skills.skill_type_id = 2002"
        set $target_language_exp_level target_language_exp_level

        db_0or1row matched_lang_experience_level "select im_categories.category_id AS matched_lang_exp_level_id, im_categories.aux_int1 AS target_lang_experience_from_sql_int, im_categories.category AS target_lang_experience_from_sql_string  from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = :user_id AND im_freelance_skills.skill_id in([ns_dbquotelist $match_target_lang_id]) AND im_freelance_skills.skill_type_id = 2002"
        set matched_lang_exp_level $target_lang_experience_from_sql_string
        set matched_lang_exp_level_id $matched_lang_exp_level_id

        # ---------------------------------------------------------------
        # Active Project Members have the relationship_id set
        # ---------------------------------------------------------------
        set object_id $user_id
        if {[lsearch $project_member_ids $user_id]>-1} {
            set selected_p 1
        } else {
            set selected_p 0
        }

        # ---------------------------------------------------------------
        # Build json array of freelancer skills with levels
        # ---------------------------------------------------------------
        set target_and_source_skill_type_ids [list 2000 2002]
        set fl_matched_target_languages_with_levels ""
        set fl_matched_target_languages_with_levels_obj_ctr 0
        set fl_languages_sql "select skill_id as matched_lang_id, skill_type_id, confirmed_experience_id, (select aux_html1 from im_categories where category_id = confirmed_experience_id) as level_style_css from im_freelance_skills where user_id =:object_id and skill_type_id in('2000', '2002')"
        set fl_matched_target_languages_with_levels_komma ",\n"
        append fl_matched_target_languages_with_levels \[\n
        db_foreach matched_lang_id $fl_languages_sql {
            set fl_matched_target_languages_with_levels_komma ",\n"
            if {0 == $fl_matched_target_languages_with_levels_obj_ctr} { set fl_matched_target_languages_with_levels_komma "" }
            set confirmed_experience_name [im_category_from_id $confirmed_experience_id]
            append fl_matched_target_languages_with_levels "$fl_matched_target_languages_with_levels_komma{\"target_language_id\": \"$matched_lang_id\",\"skill_type_id\": \"$skill_type_id\", \"target_language_name\": \"[im_category_from_id $matched_lang_id]\", \"language_level_id\": \"$confirmed_experience_id\", \"language_level_name\": \"$confirmed_experience_name\", \"level_style_css\":\"$level_style_css\"}"
            incr fl_matched_target_languages_with_levels_obj_ctr
        }
        append fl_matched_target_languages_with_levels \n\]\n


        # ---------------------------------------------------------------
        # Quality JSON
        # ---------------------------------------------------------------
        set quality_list [im_trans_freelancer_quality_json -freelancer_id $user_id]
        set quality [lindex $quality_list 1]
        
        # Find the maximum number of projects to display in the quality bar
        set num_projects [lindex $quality_list 0]
        if {$max_quality_projects < $num_projects} {
            set max_quality_projects $num_projects
        }

        set existing_assignment_status_id [db_string check_existing_assignment_status "select assignment_status_id from im_freelance_assignments fa, im_freelance_packages fp where assignee_id =:user_id and project_id =:project_id and fa.freelance_package_id = fp.freelance_package_id and assignment_status_id not in (4230) order by assignment_id desc limit 1" -default 0]

        set existing_assignment_types_names [list]        
        if {$existing_assignment_status_id ne 0} {
            set existing_assignment_types_ids [db_list check_existing_assignment_status_as_list "select assignment_type_id from im_freelance_assignments fa, im_freelance_packages fp where assignee_id =:user_id and project_id =:project_id and fa.freelance_package_id = fp.freelance_package_id order by assignment_id desc"]
        

            foreach type_id $existing_assignment_types_ids {
                set type_name [im_name_from_id $type_id]
                lappend existing_assignment_types_names $type_name
            }
            set existing_assignment_types_ids [join $existing_assignment_types_ids ","]
            set existing_assignment_types_names [join $existing_assignment_types_names ","]
        } else {
            set existing_assignment_types_ids ""
        }

        switch $format {
            html {
            append result "<tr>
                <td>$rest_oid</td>
                <td><a href=\"$url?format=html\">$project_name</a></td>
                <td>$project_manager</td>
            </tr>\n"
            }
        json {
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            set dereferenced_result ""
            foreach v $valid_vars {
                eval "set a $$v"
                regsub -all {\n} $a {\n} a
                regsub -all {\r} $a {} a
                append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
            }
            
            append dereferenced_result ", \"quality\": $quality"
            append dereferenced_result ", \"matched_target_languages_with_levels\": $fl_matched_target_languages_with_levels"

            set force_not_showing_fl_p 0
            set project_type_cat_name [im_category_from_id $project_type_id]
            set searching_for_word "Cert"
            if {$project_type_id == 2505 || [string first $project_type_cat_name $searching_for_word] > -1} {
                if {$matched_lang_exp_level_id ne 2204} {
                    set force_not_showing_fl_p 1
                }
            }

            if {$force_not_showing_fl_p ne 1} {
                append result "$komma{\"id\": \"$object_id\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}"
            }
        }
        default {}
    }
        incr obj_ctr
    }

    set project_status_id [db_string project_status_id "select project_status_id from im_projects where project_id=:project_id" -default 0]
    

    switch $format {
        html {
            set page_title "object_type: $rest_otype"
            im_rest_doc_return 200 "text/html" "
            [im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
            <tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
            </table>[im_footer]
            "
        }
        json {
            set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"project_type_id\": $project_type_id,\n\"project_status_id\": $project_status_id,\n\"message\": \"im_rest_get_custom_trans_freelancers: Data loaded..\",\n\"max_quality_projects\": $max_quality_projects,\n\"data\": \[\n$result\n\]\n}"
            im_rest_doc_return 200 "application/json" $result
            return
        }
    }
    return
}

ad_proc -public im_rest_get_custom_sencha_existing_project_mass_upload {
    -project_id
    -filename
    -filepath
    { -target_language_ids ""}
    { -uom_id 324}
    { -deadline ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls which takes care of mass upload (XLS or CSV) or Trados XML file
    
    @param project_id ProjectID for which we want to execute mass upload
    @param filename file name of uploaded file. This is generated automaticaly in Extjs
    @param filepath filepath in /tmp folder. This is generated automaticaly in Extjs
    @param target_language_ids ids of target languages for which we want to create tasks using mass upload
    @param uom_id Unit of measure id. Default is 324 (S-Word)
    @param deadline for tasks which we import using mass upload
} {

    if {$target_language_ids eq "" || $target_language_ids == 0} {
        set target_language_ids [im_target_language_ids $project_id]
    }
    
    set file_from_trados_p 0
    set task_type_manual [db_string task_type_manual "select project_type_id from im_projects where project_id=:project_id" -default 0]


    set extension [file extension $filename]

    switch $extension {
        ".xls" - ".xlsx" {
	    # Konvert the XLS to CSV for impport
	    set xls_filename "[file rootname $filepath].$extension"
	    file rename $filepath $xls_filename
            set csv_list [intranet_oo::xls_to_csv -xls_filename $xls_filename]
	    file delete $xls_filename
        }
        ".csv" {
            set chan [open $filepath]
            set csv_list [list]
            set data ""
            set separator ""
            while {![eof $chan]} {
                if {[gets $chan line] < 0} {continue}
                
                # Why skip empty lines? They may be in data. Except if the
                # buffer is empty, i.e. we are between records.
                if {$line == {} && $data == {}} {continue}
                
                append data $line
                if {![::csv::iscomplete $data]} {
                    # Odd number of quotes - must have embedded newline
                    append data \n
                    continue
                }
                   
                if {$separator eq ""} {
                    set separator [im_csv_guess_separator $data]
                }

                lappend csv_list [split $data $separator]
                set data ""
            }
            close $chan
        }
        ".xml" {
            foreach target_language_id $target_language_ids {
                set trados_created_tasks [im_trans_trados_create_tasks -project_id $project_id -trados_analysis_xml $filepath -target_language_id $target_language_id]
            }
            set file_from_trados_p 1

        }
    }

    if {!$file_from_trados_p} {

        set header_csv_fields [lindex $csv_list 0]
        set csv_files_len [llength $csv_list]
        set header_len [llength $header_csv_fields]
            
        for {set i 1} {$i < $csv_files_len} {incr i} {
            set csv_fields [lindex $csv_list $i]
            # Read one line of values and write values into local variables
            for {set j 0} {$j < $header_len} {incr j} {
                set var_name [string tolower [string trim [lindex $header_csv_fields $j]]]
                if {$var_name ne ""} {
                    set var_value [string trim [lindex $csv_fields $j]]
                    set [set var_name] $var_value
                }
            }

            if {[string trim $task_name] eq ""} {
                continue
            }
                
            # Create the task

            set task_ids [im_task_insert $project_id $task_name $file_name $units $uom_id $task_type_manual $target_language_ids]

            # Convert the deadline to something we can read
            if {$deadline ne ""} {
                if {![catch {clock_to_ansi [clock scan "$deadline"]} deadline]} {
                    foreach task_id $task_ids {
                        db_dml update_end_date "update im_trans_tasks set end_date = :deadline where task_id = :task_id"
                    }
                }
            }
        }

    }

    set result "{\"success\": true}"
    im_rest_doc_return 200 "application/json" $result
    return

}

ad_proc -public im_rest_get_custom_sencha_remove_existing_project_task {

    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
     Handler for GET rest calls to remove existing trans task
} {


    array set query_hash $query_hash_pairs

    if {[info exists query_hash(task_id)]} {
        set task_id $query_hash(task_id)
        set tasks_ids $task_id
    } else {
        set task_id ""
        set tasks_ids ""
    }
    
    set result false
    set message "You must provide task_id"

    if {$task_id ne ""} {

        # preparing variable which will count number of removed tasks
        set number_of_removed_tasks 0
        set number_of_not_removed_tasks 0

        # preparing variables for removed and not removed tasks
        set removed_tasks_ids [list]
        set not_removed_tasks_names [list]

        foreach task_id [split $tasks_ids ","] {

            # getting task name, useful later
            set current_task_name [db_string current_task_name "select task_name from im_trans_tasks where task_id =:task_id" -default ""]

            # get project_id, this will be useful later in code
            set project_id [db_string project_id "select project_id from im_trans_tasks where task_id =:task_id" -default ""]
        
            # setting default "permission" var
            set removing_task_possible_p 0
            set total_tasks_existing_assignments 0
    
            # checking if we already have assignment, first we get task package
            set task_packages [db_list task_packags "select freelance_package_id from im_freelance_packages_trans_tasks where trans_task_id =:task_id"]
 
            # list of freelance package ids which we can later delete
            set freelance_packages_to_be_deleted_ids [list]

            foreach freelance_package_id $task_packages {
                set count_existing_assignments_for_package [db_string count_existing_assignments_for_package "select count(*) from im_freelance_assignments where freelance_package_id =:freelance_package_id" -default 0]
                if {$count_existing_assignments_for_package > 0} {
                    incr total_tasks_existing_assignments
                } else {
                    lappend freelance_packages_to_be_deleted_ids $freelance_package_id
                }
            }
    
            if {$total_tasks_existing_assignments eq 0} {
                set removing_task_possible_p 1
            } 

            set files_to_be_removed [list]
    
            if {$removing_task_possible_p eq 1} {
            # removing tasks
                set del "_del"
                set new_task_name [concat $current_task_name$del]
                # check if task_name_del exist
                set task_name_del_count [db_string task_name_del_possible_sql "select count(*) from im_trans_tasks where task_name=:new_task_name and project_id=:project_id" -default 0]
                if {$task_name_del_count > 0} {
                    set random_sring "_[ad_generate_random_string]"
                    set new_task_name [concat $new_task_name$random_sring]
                }
                db_dml update_task_status "update im_trans_tasks set task_status_id = 372, task_name =:new_task_name where task_id =:task_id"
                # also removing tasks packages
                foreach freelance_package_id $freelance_packages_to_be_deleted_ids {
                    db_dml delete_unassigned_package "delete from im_freelance_packages where freelance_package_id =:freelance_package_id"
                }

               # remove file from filestorage, only if no more tasks for different lagnauges exist
               set other_language_tasks [db_string other_language_tasks "select count(*) from im_trans_tasks where task_name=:current_task_name and project_id =:project_id and task_status_id <> 372" -default 0]
               if {$other_language_tasks == 0} {
                   set source_folder [im_trans_task_folder -project_id $project_id -folder_type source]
                   set project_dir [cog::project::path -project_id $project_id]
                   set source_dir "${project_dir}/$source_folder"
                   # get filename and then remove it
                   set file_to_be_removed [db_string file_to_be_removed "select task_filename from im_trans_tasks where task_id =:task_id" -default ""]
                   # other tasks for different language still exist ?
                   if {$file_to_be_removed ne ""} {
                      set file_to_be_removed_fullpath "${source_dir}/$file_to_be_removed"
                      file delete $file_to_be_removed_fullpath
                   }
               }
                lappend removed_tasks_ids $task_id
                incr number_of_removed_tasks
                set message "Task id: $task_id status changed to 'Deleted'"
                set result true
            } else {
                lappend not_removed_tasks_names $current_task_name
                incr number_of_not_removed_tasks
            }

            set not_removed_tasks_msg ""
            if {$number_of_not_removed_tasks > 0} {
                foreach not_removed_task_name $not_removed_tasks_names {
                    set not_removed_tasks_msg "$not_removed_tasks_msg <b>$not_removed_task_name</b></br>"
                }
            }

            # setting result message
            if {$number_of_not_removed_tasks > 0} {
                set message "<p>Tasks:</p> $not_removed_tasks_msg <p>Could not be removed</p>";
            } else {
                if {$number_of_removed_tasks > 0} {
                    # parse message to be nicely readable
                    set message "All selected tasks were removed";
                } else {
                    set message "Tasks not removed";
                }
            }
        }
    }

    set result "{\"success\": $result, \"message\":\"[im_quotejson $message]\", \"number_of_removed_tasks\":$number_of_removed_tasks,\"number_of_not_removed_tasks\":$number_of_not_removed_tasks }"
    
    im_rest_doc_return 200 "application/json" $result
    return
}



ad_proc -public im_rest_get_custom_sencha_task_action {
    -task_ids
    -action_type
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on project freelancer tasks
    
    @param task_ids comma seperated tasks_ids for which we want to execute given 'action_type'

    @return action_type Action type which user requested
    @return result true/false if required action_type was a success or failure

} {

    set result true
    set error "no error"
    set redirect_url ""

    # Getting project_id using first task found in 'task_ids'
    set first_task_id [lindex $task_ids 0]
    set project_id [db_string project_id_from_task_id "select project_id from im_trans_tasks where task_id =:first_task_id limit 1" -default 0]

    if {$project_id eq 0} {

       set error "Wrong project_id"

    } else {

        # Our behavior depends on give 'action_type'
        switch $action_type {

            "new_quote" {
                set invoice_id [im_trans_invoice_create_from_tasks -project_id $project_id -task_ids $task_ids]
                set redirect_url "[ad_url][apm_package_url_from_key "intranet-invoices"]view?invoice_id=$invoice_id"
                set result_message "Created new quote: $invoice_id"
            }

           "save_replace_quote" {
                set invoice_id [db_string quote "select max(cost_id) from im_costs where project_id = :project_id and cost_type_id = [im_cost_type_quote]" -default ""]
                if {$invoice_id ne ""} {
                    set invoice_id [im_trans_invoice_create_from_tasks -project_id $project_id -invoice_id $invoice_id -task_ids $task_ids]
                    set result_message "Saved quote: $invoice_id"
                } else {
                    set invoice_id [im_trans_invoice_create_from_tasks -project_id $project_id -task_ids $task_ids]
                    set result_message "Created new quote: $invoice_id"
                }
                set redirect_url "[ad_url][apm_package_url_from_key "intranet-invoices"]view?invoice_id=$invoice_id"
            }

            "delete_quote" {
                set result_message "delete_quote"
            }

            default {
                set result false
                set result_message "unknown action type"
                set error "unknown action type"
            }

        }

    }

    set result "{\"success\": $result, \"message\":\"$result_message\", \"error\":\"$error\", \"action_type\":\"$action_type\", \"task_ids\":\"$task_ids\", \"project_id\":$project_id, \"redirect_url\":\"$redirect_url\" }"
    
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_rest_get_custom_sencha_project_tasks {
    -project_id
    { -freelancer_ids "" }
    { -lang_id "" }
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on project freelancer tasks
    
    @param project_id ProjectID for which to look for tasks
    @param freelancer_ids List of freelancers, which are selected to return the tasks
    @param lang_id
} {

    if {[llength $freelancer_ids]>0} {
        
        set freelancer_lang_ids [list]
    foreach freelancer_id $freelancer_ids {
            set project_languages [im_freelancer_project_langs -freelancer_id $freelancer_id -project_id $project_id]
            foreach project_lang_id $project_languages {
                set language_sibligns [im_get_language_siblings -language_id $project_lang_id]
                foreach sibling_id $language_sibligns {
                    lappend project_languages $sibling_id
                }
            }
            set freelancer_target_lang_id($freelancer_id) [lsort -unique $project_languages]
        }

        # Append already assigned freelancers if they are translating in the language of the selected freelancer
    # Actually not needed at all. If we select one or multiple freelancer but hide unselectable packages
    # We don't need to display all of the freelancers.

    
#        db_foreach assignee "select distinct assignee_id from im_freelance_assignments fa, im_freelance_packages fp, im_freelance_skills fs
#            where fp.project_id = :project_id 
#            and fp.freelance_package_id = fa.freelance_package_id 
#            and fa.assignment_status_id <> 4230
#            and fa.assignee_id = fs.user_id
#            and fs.skill_id in ([ns_dbquotelist $freelance_lang_ids]) and fs.skill_type_id=2002
#            and fa.assignee_id not in ([ns_dbquotelist $freelancer_ids])" {
#                lappend freelancer_ids $assignee_id
#                set freelancer_target_lang_id($assignee_id) [im_freelancer_project_langs -freelancer_id $assignee_id -project_id $project_id]
#        }
    } else {
        # No freelancer selected, show all current assignments
        db_foreach assignee "select distinct assignee_id from im_freelance_assignments fa, im_freelance_packages fp where project_id = :project_id and fp.freelance_package_id = fa.freelance_package_id and fa.assignment_status_id <> 4230" {
            lappend freelancer_ids $assignee_id
            set freelancer_target_lang_id($assignee_id) [im_freelancer_project_langs -freelancer_id $assignee_id -project_id $project_id]
        }
    }

    # getting project info
    db_1row project_info "select to_char(start_date,'YYYY-MM-DD HH24:MI') as project_start_date, project_status_id,company_id, subject_area_id,source_language_id from im_projects where project_id = :project_id"


    set task_type_ids [list]
    set task_uom_ids [list]

    db_foreach task_names {
        select t.*, c.aux_string1
        from im_trans_tasks t, im_categories c
        where t.project_id = :project_id
        and c.category_id = t.task_type_id
    } {
        lappend task_ids $task_id
        lappend task_lang_arr($target_language_id) $task_id
        lappend target_language_ids $target_language_id
        if {false} {
            set pdf_file [im_trans_task_source_file_pdf -task_id $task_id]
            set file_exists($task_id) [file exists $pdf_file]
        }
        set task_name_arr($task_id) "$task_name"
        set task_type_arr($task_id) ""
    
        if {[lsearch $task_uom_ids $task_uom_id]<0} {lappend task_uom_ids $task_uom_id}
        if {$task_units eq ""} {set task_units 0}
        set task_units_arr($task_id) $task_units
        set task_uom_arr($task_id) "($task_units [im_category_from_id $task_uom_id])"
    
        # Add the task type ids
        foreach task_type $aux_string1 {
            set task_type_id [db_string type_id "select category_id from im_categories where lower(category) = :task_type and category_type = 'Intranet Trans Task Type'" -default ""]
            if {$task_type_id ne ""} {
                lappend task_type_arr($task_id) $task_type_id
                if {[lsearch $task_type_ids $task_type_id]<0} {lappend task_type_ids $task_type_id}
            
                if {[set ${task_type}_id] ne ""} {
                    set assigned_freelancer_id  [set ${task_type}_id]
                    # Who is assigned to a task (as we can have multiple for this task
                    set ${task_type}_assigned_arr($task_id) $assigned_freelancer_id
                
                    # What tasks is the freelancer already assigned, so he can't be assigned for it anywhere else
                    if {[info exists assigned_tasks($assigned_freelancer_id)]} {
                        lappend assigned_tasks($assigned_freelancer_id) $task_id
                    } else {
                        set assigned_tasks($assigned_freelancer_id) $task_id
                    }
                } 
            }
        }
    }

    set package_task_ids [list]
    # build list with non repeated languages
    set non_repeated_target_languages [lsort -unique $target_language_ids]
    
    # we build list containing all existing assignments so we can later force_greyout when building actual json
    foreach freelancer_id $freelancer_ids {

        set freelancer_already_assigned_packages [list]

        foreach target_language_id $non_repeated_target_languages {
            foreach task_type_id $task_type_ids {
                set package_assigned_sql "select fp.freelance_package_id, freelance_package_name, fa.assignment_status_id,
                    end_date, assignment_id, rate as assignment_rate, uom_id,
                    coalesce(assignment_units,0) as assignment_units, purchase_order_id,
                    (select target_language_id 
                        from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt 
                        where tt.task_id = fptt.trans_task_id
                        and fptt.freelance_package_id = fp.freelance_package_id limit 1
                    ) as package_language_id
                    from im_freelance_assignments fa, im_freelance_packages fp
                    where assignee_id = :freelancer_id
                    and assignment_type_id = :task_type_id
                    and fa.freelance_package_id = fp.freelance_package_id
                    and project_id = :project_id
                    and fa.assignment_status_id <> 4230"


                db_foreach package_assigned $package_assigned_sql {
                    if {$package_language_id eq $target_language_id} {
                        lappend freelancer_already_assigned_packages [list $freelancer_id $target_language_id $task_type_id $freelance_package_id $assignment_status_id]
                    }

                }
            }

        }

    }

    # Find all freelance_package_ids with purchase_order so we can hide them afterards
    set freelance_packages_with_po  [db_list po_created "select fa.freelance_package_id
         from im_freelance_assignments fa, im_freelance_packages fp
         where fa.freelance_package_id = fp.freelance_package_id
         and fp.project_id = :project_id
         and fa.assignment_status_id <> 4230
         and fa.purchase_order_id is not null"] 
    
    # buidling freelancer array
    foreach freelancer_id $freelancer_ids {

        foreach target_language_id $non_repeated_target_languages {
            # Necessary defaults
            if {![info exists assigned_tasks($freelancer_id)]} {
                set assigned_tasks($freelancer_id) [list]
            }

            # looping thru all project task types (e.g. 'Trans + Edit' or 'Trans + Edit + Proof')
            foreach task_type_id $task_type_ids {

                set task_type [im_category_from_id -translate_p 0 $task_type_id]

                set task_sum_units 0

                # building currently existing assignments json
                set package_task_ids [list]

                set freelancer_already_added_packages [list]

                set package_assigned_sql "select fp.freelance_package_id, freelance_package_name, fa.assignment_status_id,
                    end_date, assignment_id, rate as assignment_rate, uom_id,
                    coalesce(assignment_units,0) as assignment_units, purchase_order_id
                    from im_freelance_assignments fa, im_freelance_packages fp, im_freelance_packages_trans_tasks fptt
                    where assignee_id = :freelancer_id
                    and assignment_type_id = :task_type_id
                    and fa.freelance_package_id = fp.freelance_package_id
                    and project_id = :project_id
                    and fp.freelance_package_id = fptt.freelance_package_id
                    and fa.assignment_status_id <> 4230"

                db_foreach package_assigned $package_assigned_sql {

                    set count_assigned_packages 0
            
                    set ${task_type_id}_total_count($freelancer_id) 0
                    set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
                    set package_filenames ""

                    db_1row package_language "select tt.target_language_id as package_language from im_trans_tasks tt, im_freelance_packages_trans_tasks fpt where tt.task_id = fpt.trans_task_id and freelance_package_id =:freelance_package_id limit 1"

                    set count_package_filenames 0
                    set already_assigned_package_filenames($freelancer_id)  [list]
                    if {$package_language eq $target_language_id && [lsearch $freelancer_already_added_packages $freelance_package_id] eq -1 && $package_language eq $target_language_id} {
                        foreach task_id $task_ids {
                            set package_filenames_komma ","
                            if {$count_package_filenames == 0} {
                                set package_filenames_komma ""
                            }
                            append package_filenames "$package_filenames_komma$task_name_arr(${task_id})"
                            lappend assigned_tasks($freelancer_id) $package_filenames
                            incr count_package_filenames
                            lappend package_task_ids $task_id
                        }

                        set po_created_p [db_0or1row po_created "select c.amount,i.invoice_id,i.invoice_nr, sum(item_units) as item_units, price_per_unit, company_contact_id, c.currency from im_invoice_items ii, im_invoices i, im_costs c, im_trans_tasks t where i.invoice_id = ii.invoice_id and t.task_id in (select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id) and t.task_id = ii.task_id and company_contact_id = :freelancer_id and c.cost_id = i.invoice_id group by c.amount, i.invoice_id, invoice_nr, price_per_unit, company_contact_id, c.currency limit 1"]
                        if {$po_created_p} {
                            set po_exist_p true
                            set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id return_url}]
                            set invoice_total_price "$amount $currency"
                        } else {
                            set po_exist_p false
                            set invoice_url ""
                            set invoice_total_price 0
                        }
                        
                        # force_unviability_p  is not used anymore, should be removed later
                        set force_unviability_p 0
                        set force_grey_out_p 0

                        set status_id $assignment_status_id
                        
                        if {$status_id eq 4228} {
                            set force_unviability_p 1
                        }

                        set count_same_freelancer_other_task_types 0
                        set count_other_freelancer_same_task_type 0
                        set count_same_freelancer_accepted_tasks 0

                        foreach same_package_other_task_type_assignemnt $freelancer_already_assigned_packages {
                            set other_assignment_freelancer_id [lindex $same_package_other_task_type_assignemnt 0]
                            set other_assignment_target_language_id [lindex $same_package_other_task_type_assignemnt 1]
                            set other_assignment_task_type_id [lindex $same_package_other_task_type_assignemnt 2]
                            set other_assignment_package_id [lindex $same_package_other_task_type_assignemnt 3]
                            set other_assignment_status_id [lindex $same_package_other_task_type_assignemnt 4]    
                            if {$other_assignment_freelancer_id eq $freelancer_id && $other_assignment_target_language_id eq $package_language && $task_type_id ne $other_assignment_task_type_id} {
                                if {$other_assignment_status_id eq 4222 || $other_assignment_status_id eq 4220 } {
                                    incr count_same_freelancer_other_task_types
                                }
                            }
                            if {$other_assignment_target_language_id eq $package_language && $task_type_id eq $other_assignment_task_type_id} {
                                if {$other_assignment_status_id eq 4222 || $other_assignment_status_id eq 4220 } {
                                    incr count_other_freelancer_same_task_type
                                }
                            }
                            if {$assignment_status_id eq 4221} {
                                if {$other_assignment_freelancer_id eq $freelancer_id && $other_assignment_target_language_id eq $package_language} {
                                    incr count_same_freelancer_accepted_tasks
                                }
                            }
                        }
                        # if status is not 4221(Requested), we are not forcing greying out, because we need possiblity to request also other freelaners
                        if {$assignment_status_id ne 4221} {
                            if {$count_same_freelancer_other_task_types > 0 || $count_other_freelancer_same_task_type > 0 || $count_same_freelancer_accepted_tasks > 0} {
                                if {$assignment_status_id ne 4222} {
                                    set force_grey_out_p 1
                                    set force_unviability_p 1
                                }
                            }
                        }

                        if {$assignment_status_id ne 4228} {
                            lappend ${task_type_id}_${target_language_id}_fl_assignments($freelancer_id) "{\"package_id\":$freelance_package_id, \"package_name\": \"$freelance_package_name\",  \"package_language\": \"$package_language\", \"assignment_status_id\":$status_id, \"package_filenames\":\"[im_quotejson $package_filenames]\", \"assignment_rate\":\"$assignment_rate\", \"task_type_id\":$task_type_id, \"assignment_units\":\"$assignment_units\", \"assignment_uom\":\"[im_category_from_id $uom_id]\", \"package_task_ids\":\"$package_task_ids\", \"assigned\":true, \"po_exist\":$po_exist_p, \"invoice_url\":\"$invoice_url\", \"invoice_total_amount\":\"$invoice_total_price\", \"force_grey_out\":\"$force_grey_out_p\"}"
                            lappend freelancer_already_added_packages $freelance_package_id
                            incr ${task_type_id}_total_count($freelancer_id)
                        }


                    }

                }

        # building potential assignments
        # Hide packages with PO
        if {$freelance_packages_with_po ne ""} {
            set po_sql  "and fpt.freelance_package_id not in ([ns_dbquotelist $freelance_packages_with_po])"
        } else {
            set po_sql ""
        }

        set package_task_ids [list]
        db_foreach package "select tt.target_language_id as package_language, fp.freelance_package_id, freelance_package_name from im_freelance_packages fp, im_freelance_packages_trans_tasks fpt, im_trans_tasks tt where tt.task_id = fpt.trans_task_id and package_type_id = :task_type_id and fpt.freelance_package_id = fp.freelance_package_id and tt.project_id =:project_id and tt.target_language_id =:target_language_id $po_sql" {
                        
                        # making sure we are not adding same package more than once
                        db_1row package_language "select tt.target_language_id as package_language from im_trans_tasks tt, im_freelance_packages_trans_tasks fpt where tt.task_id = fpt.trans_task_id and freelance_package_id =:freelance_package_id limit 1"
                        if {[lsearch $freelancer_already_added_packages $freelance_package_id] eq -1 && $package_language eq $target_language_id} {

                            set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
                
                            set package_filenames ""
                            set count_package_filenames 0
                            set package_task_ids ""
                            foreach task_id $task_ids {
                                set package_filenames_komma ","
                                if {$count_package_filenames == 0} {
                                    set package_filenames_komma ""
                                }
                                set task_sum_units [expr $task_sum_units + $task_units_arr($task_id)]
                                append package_filenames "$package_filenames_komma$task_name_arr(${task_id})"
                                lappend package_task_ids $task_id
                                incr count_package_filenames
                            }

                            set count_same_freelancer_other_task_types 0
                            set count_other_freelancer_same_task_type 0
                            set count_same_freelancer_accepted_tasks 0
                        
                            foreach same_package_other_task_type_assignemnt $freelancer_already_assigned_packages {
                                set other_assignment_freelancer_id [lindex $same_package_other_task_type_assignemnt 0]
                                set other_assignment_target_language_id [lindex $same_package_other_task_type_assignemnt 1]
                                set other_assignment_task_type_id [lindex $same_package_other_task_type_assignemnt 2]
                                set other_assignment_package_id [lindex $same_package_other_task_type_assignemnt 3]
                                set other_assignment_status_id [lindex $same_package_other_task_type_assignemnt 4]    
                                if {$other_assignment_freelancer_id eq $freelancer_id && $other_assignment_target_language_id eq $package_language && $task_type_id ne $other_assignment_task_type_id && $other_assignment_package_id eq $freelance_package_id} {
                                    if {$other_assignment_status_id eq 4222 || $other_assignment_status_id eq 4220 } {
                                        incr count_same_freelancer_other_task_types
                                    }
                                }
                                if {$other_assignment_target_language_id eq $package_language && $task_type_id eq $other_assignment_task_type_id} {
                                    if {$other_assignment_status_id eq 4222 || $other_assignment_status_id eq 4220 } {
                                        if {$other_assignment_freelancer_id eq $freelancer_id} {
                                            incr count_other_freelancer_same_task_type
                                        }
                                    }
                                }
                                if {$assignment_status_id eq 4221} {
                                    if {$other_assignment_freelancer_id eq $freelancer_id && $other_assignment_target_language_id eq $package_language} {
                                        incr count_same_freelancer_accepted_tasks
                                    }
                                }
                            }
                            
                            set force_grey_out_p 0
                            set force_unviability_p 0


                            if {$count_same_freelancer_other_task_types > 0 || $count_other_freelancer_same_task_type > 0 || $count_other_freelancer_same_task_type >0 } {
                                set force_grey_out_p 1
                            }

                            set assignable true
                            lappend freelancer_already_added_packages $freelance_package_id
                            set status_id 0
                            lappend ${task_type_id}_${target_language_id}_fl_assignments($freelancer_id) "{\"package_id\":$freelance_package_id, \"package_name\": \"$freelance_package_name\",  \"package_language\": \"$package_language\", \"assignment_status_id\":$status_id ,\"package_filenames\":\"[im_quotejson $package_filenames]\", \"package_task_ids\":\"$package_task_ids\",\"assigned\":false, \"task_type_id\":$task_type_id, \"assignable\":$assignable, \"po_exist\":false,\"force_grey_out\":$count_other_freelancer_same_task_type}"
                        }
                    }
            }

        }

    }

    set tasks ""
    set obj_ctr 0

    foreach freelancer_id $freelancer_ids {

        foreach lang_id $freelancer_target_lang_id($freelancer_id) {
            
           
        
            set already_used_packages [list]
            
            set target_language_exist_in_project [db_string target_language_exist_in_project "select count(*) from im_trans_tasks where project_id =:project_id and target_language_id =:lang_id and task_status_id <> 372" -default 0]
	    if {$target_language_exist_in_project > 0} {
            set freelancer_name [im_name_from_user_id_helper $freelancer_id]


            if {[info exists freelancer_target_lang_id($freelancer_id)]} {
                set target_language_id $lang_id
                set target_language [im_category_from_id $lang_id]
            } else {
                set target_language ""
            }

            set freelancer_task_types ""
            set freelancer_task_types_obj_ctr 0

            set freelancer_task_types ""

            foreach task_type_id $task_type_ids {

                set package_added_p 0

                set freelancer_task_types_komma ",\n"
                if {0 == $freelancer_task_types_obj_ctr} { 
                    set freelancer_task_types_komma "" 
                }

                set freelancer_task_files ""
            
                set task_packages ""
                set task_type_name [im_category_from_id $task_type_id]

                set fl_tasks ""

                if {[info exists ${task_type_id}_${lang_id}_fl_assignments($freelancer_id)]} {
                   # set fl_tasks [set ${task_type_id}_possibilities($freelancer_id)]
                   set fl_tasks [set ${task_type_id}_${lang_id}_fl_assignments($freelancer_id)]
                }


                set merged_tasks ""
                set merged_tasks_count 0

                foreach task_to_be_merged $fl_tasks {

                    set merged_tasks_komma ",\n"
                    if {$merged_tasks_count == 0} {
                        set merged_tasks_komma ""
                    }

                    append merged_tasks $merged_tasks_komma$task_to_be_merged
                    incr merged_tasks_count
                    set package_added_p 1

                }

                append freelancer_task_types "$freelancer_task_types_komma{\"task_type_id\":\"$task_type_id\",\"task_type_name\":\"$task_type_name\", \"task_files\": \[\n$merged_tasks\n\]}"
                incr freelancer_task_types_obj_ctr

            }        

            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
        
                set single_row "$komma{\"freelancer_id\": \"$freelancer_id\", \"freelancer_name\": \"$freelancer_name\", \"target_language_id\":\"$target_language_id\", \"target_language\": \"$target_language\", \"project_status_id\": \"$project_status_id\",  \"task_types\": \[\n$freelancer_task_types\n\]}"
                append tasks $single_row 
                incr obj_ctr        

            }

        }
    }


    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"data\": \[\n$tasks\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_assignment_messages {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls which returns assignment messages exchanged between Project Manager and Freelancer
} {

    if {[info exists query_hash(assignee_id)]} {
        set assignee_id $query_hash(assignee_id)
    } 
    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    } 
    set assignee_id 332860
    set project_id 648320
    set mail_context_ids [db_list assignments "select assignment_id 
        from im_freelance_assignments fa, im_freelance_packages fp
        where fa.freelance_package_id = fp.freelance_package_id
        and fa.assignee_id = :assignee_id
        and fp.project_id = :project_id"]

    set context_sql_where "context_id in ([ns_dbquotelist $mail_context_ids])"
    
    set messages_sql "select message_id, sender_id, im_name_from_id(sender_id) as sender_name, from_addr, package_id,  sent_date, body, subject, context_id, to_addr, log_id
        from acs_mail_log 
        where $context_sql_where"
    
    set messages ""
    set obj_ctr 0

    db_foreach message $messages_sql {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        if {![exists_and_not_null $message_id]} {
            set message_id "no id"
        }

        if {$rest_user_id eq $sender_id} {
            set sender_display "You"
        } else {
            set sender_display $sender_name
        }

        append messages "$komma{\"object_id\":\"$message_id\", \"message_id\":\"$message_id\", \"sent_date\":\"$sent_date\", \"sender_id\":$sender_id, \"sender_name\":\"[im_quotejson $sender_name]\", \"sender_display\":\"[im_quotejson $sender_display]\", \"to_addr\":\"[im_quotejson $to_addr]\", \"subject\":\"[im_quotejson $subject]\", \"body\":\"[im_quotejson $body]\"}"
        incr obj_ctr
    }

    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"data\": \[\n$messages\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_assign_freelancer {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls which assigns freelancer to project task
} {

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(task_id)]} {
        set task_id $query_hash(task_id)
    } 

    if {[info exists query_hash(assignee_id)]} {
        set assignee_id $query_hash(assignee_id)
    } 

    if {[info exists query_hash(assignment_type_id)]} {
        set assignment_type_id $query_hash(assignment_type_id)
    } 

    if {[info exists query_hash(units)]} {
        set units $query_hash(units)
    } else {
        set units ""
    }

    if {[info exists query_hash(uom)]} {
        set uom $query_hash(uom)
    }  else {
        set uom ""
    }

    if {[info exists query_hash(deadline)]} {
        set deadline $query_hash(deadline)
    }  else {
        set deadline ""
    }

    if {[info exists query_hash(assignment_comment)]} {
        set assignment_comment $query_hash(assignment_comment)
    }  else {
        set assignment_comment ""
    }

    set data ""
    set newly_created_assignment_id ""
    catch {
        set newly_created_assignment_id [im_freelance_assignment_create -task_ids $task_id -assignee_id $assignee_id -assignment_type_id $assignment_type_id -assignment_units $units -uom_id $uom -end_date $deadline -assignment_comment $assignment_comment]
    } errmsg

    if {$newly_created_assignment_id eq ""} {
        set success false
        append data "{\n\"error\": \"[im_quotejson $errmsg]\"}"
    } else {
        set success true
        append data "{\n\"newly_created_assignment_id\": \"$newly_created_assignment_id\"}"
    }

    
    set result "{\"success\": $success, \n\"data\": \[\n$data\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}


ad_proc -public im_rest_get_custom_sencha_assignment_message {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls which sends message
} {

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    } 

    if {[info exists query_hash(assignee_id)]} {
        set assignee_id $query_hash(assignee_id)
    } 

    if {[info exists query_hash(subject)]} {
        set subject $query_hash(subject)
    }

    if {[info exists query_hash(body)]} {
        set body $query_hash(body)
    }

    set sender_id $rest_user_id

    set recipient_ids [list 332860]

    set assignment_id [db_string assignment "select min(assignment_id)
        from im_freelance_assignments fa, im_freelance_packages fp
        where fa.freelance_package_id = fp.freelance_package_id
        and fp.project_id = :project_id
        and fa.assignee_id = :assignee_id
         " -default ""]

    set package_id [ad_conn package_id]
        
    if {[exists_and_not_null revision_id]} {
        set file_ids $revision_id
    } else {
        set file_ids [list]
    }
        
    set log_id [im_freelance_message_send \
        -sender_id $sender_id \
        -recipient_ids $recipient_ids \
        -subject "$subject" \
        -body "$body" \
        -context_id $assignment_id \
        -file_ids $file_ids \
        -package_id $package_id \
        -project_id $project_id]

        im_freelance_record_view -object_id $log_id -viewer_id $rest_user_id

        util_user_message -html -message "Message recorded"

        set success true
        set data ""

    
    set result "{\"success\": $success, \n\"data\": \[\n$data\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}





ad_proc -public im_rest_get_custom_sencha_create_packages {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call which creates packages
} {
    set message ""
    set locale [lang::user::locale -user_id $rest_user_id]

    # check for parameters
    array set query_hash $query_hash_pairs
    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
        set success true
    } else {
        set success false
        set message "project_id does not exist"
    }

    # check if the project already exist
    set project_exist_p [db_string project_exists "select 1 from im_projects where project_id =:project_id" -default 0]
    if {$project_exist_p eq 0} {
        set success false
        set message "Project: $project_id does not exist"
    }

    # get the target languages
    if {$success} { 

        set sql "
            select distinct target_language_id
            from im_trans_tasks
            where project_id=:project_id
            and task_status_id <> 372
            "
        set target_language_ids [db_list target_language_ids $sql]
    
    } else {
        set target_language_ids [list]
    }

    set newly_created_packages [list]

    # loop thru all the target languages and task types to create packages
    foreach target_language_id $target_language_ids {
            set task_type_ids [db_list task_types "select task_type_id from im_trans_tasks where project_id =:project_id and target_language_id =:target_language_id and task_status_id != 372"]
    
            if {[llength $task_type_ids] > 0} {

                foreach task_type_id $task_type_ids {

                    catch {
                        set task_ids [db_list task_ids "select task_id from im_trans_tasks where task_type_id =:task_type_id and target_language_id =:target_language_id and project_id =:project_id and task_status_id <> 372"]         
                        # translate the task type id into tasks steps
                        set task_types [db_string task_types "select aux_string1 from im_categories where category_id =:task_type_id"]
                        
                        foreach task_type [split [string trim $task_types] " "] {
                            # this goes thru each of the steps and checks if we have a package already created if not create it
                            set package_type_id [db_string package_type_id "select category_id from im_categories where category_type = 'Intranet Trans Task Type' and lower(category) =:task_type"]
                            set freelance_package_exist_p [db_string freelance_package_exist "select 1 from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt, im_trans_tasks tt 
                                where fp.freelance_package_id = fptt.freelance_package_id and fptt.trans_task_id = tt.task_id
                                and tt.target_language_id =:target_language_id
                                and package_type_id =:package_type_id and fp.project_id =:project_id limit 1" -default 0]
                            if {$freelance_package_exist_p eq 0} {
                                set new_freelance_package_id [im_freelance_package_create -trans_task_ids $task_ids -package_type_id $package_type_id]
                                lappend newly_created_packages $new_freelance_package_id  
                                #im_translation_update_project_dates -project_id $project_id -task_ids $task_ids
                                #im_freelance_calculate_deadline -freelance_package_id $new_freelance_package_id -update_deadline_p 1
                            } else {
                                # we can still have tasks not assigned to any package (e.g. added later, after packages were created)
                                # so we need to make sure we 'pack' that tasks into seperate packages
                                set tasks_added_later [list]
                                foreach task_added_later_id $task_ids {
                                    set count_single_tasks [db_string count_single_tasks "select count(fpt.trans_task_id) from im_freelance_packages_trans_tasks fpt, im_trans_tasks tt, im_freelance_packages fp where tt.task_id = fpt.trans_task_id and fp.freelance_package_id = fpt.freelance_package_id and tt.project_id =:project_id and tt.target_language_id =:target_language_id and fp.package_type_id =:package_type_id and fpt.trans_task_id =:task_added_later_id" -default 1]
                                    if {$count_single_tasks eq 0} {
                                        lappend tasks_added_later $task_added_later_id
                                        #im_translation_update_project_dates -project_id $project_id -task_ids $task_ids
                                    }
                                }
                                if {[llength $tasks_added_later] > 0} {
                                    set new_freelance_package_id [im_freelance_package_create -trans_task_ids $tasks_added_later -package_type_id $package_type_id]
                                }
                            }
                        }
                    } errmsg

                    if {[string length $errmsg] > 0} {
                        sencha_errors::add_error -object_id $project_id -field "Project [im_name_from_id $project_id]: [lang::message::lookup $locale "sencha-assignment.could_not_create_package"]" -problem "$errmsg"
                    }

                }
            } else {
                set errmsg [lang::message::lookup $locale "sencha-assignment.please_create_tasks_first"]
                sencha_errors::add_error -object_id $project_id -field "<b>Project [im_name_from_id $project_id]</b>: [lang::message::lookup $locale "sencha-assignment.could_not_create_package"]" -problem $errmsg
            }
    }

    sencha_errors::display_all_existing_errors_and_warnings -object_ids [list $project_id] -modal_title [lang::message::lookup $locale "sencha-assignment.could_not_create_package"] -action_type "redirect"

    set result "{\"success\": $success, \"message\": \"$errmsg\",\n\"data\": \[\n[join $newly_created_packages ","]\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}

ad_proc -public im_rest_get_custom_sencha_assignment_potential_data {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call which returns assignment potential deadline and rate for given @param feelancer_id and @param freelance_package_id
} {

    # check for parameters
    array set query_hash $query_hash_pairs

    if {[info exists query_hash(freelance_package_id)]} {
        set freelance_package_id $query_hash(freelance_package_id)  
    } 

    if {[info exists query_hash(freelancer_id)]} {
        set freelancer_id $query_hash(freelancer_id)  
    } 

    # freelance_package_id can be either single value and list of comma seperated values
    set freelance_package_id_list [split $freelance_package_id ","]

    # get only existing freelance packages, this way we prevent from querying for non existig packages
    set freelance_package_id_list [db_list existing_freelance_packages "select freelance_package_id from im_freelance_packages where freelance_package_id in([ns_dbquotelist $freelance_package_id_list])"]

    # empty var to store rates, deadlines, units, uom for all given freelance_pacakge_ids
    set packages_rate_and_deadlines ""
    set obj_ctr 0

    if {[llength $freelance_package_id_list]>0} {
    set package_type_id [db_string package_type_id "select package_type_id from im_freelance_packages where freelance_package_id in([ns_dbquotelist $freelance_package_id_list]) limit 1" -default ""]
    set package_task_ids [db_list package_tasks_ids "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id in ([ns_dbquotelist $freelance_package_id_list])"]
    set task_type [db_string package_type_id "select lower(category) as task_type from im_categories where category_type = 'Intranet Trans Task Type' and category_id =:package_type_id" -default ""]
    } else {
    # Throw an error
    }

    # we need provider id (company of the freelancer)
    set provider_id [db_string company_id "select c.company_id
            from
                im_companies c,
                acs_rels r
            where
                r.object_id_two = :freelancer_id
            and r.object_id_one = c.company_id order by company_id desc limit 1" -default ""]

    if {$provider_id eq ""} {
        set provider_id [im_company_freelance]
    }

    # ---------------------------------------------------------------
    # Deadline calculation
    # ---------------------------------------------------------------
    
    set previous_assignment_id [db_string assignment_exist "select assignment_id as previous_assignment_id from im_freelance_assignments where freelance_package_id in([ns_dbquotelist $freelance_package_id_list]) and assignee_id =:freelancer_id limit 1" -default 0]
    if {$previous_assignment_id ne 0} {
        set deadline [im_freelance_calculate_deadline -assignment_id $previous_assignment_id]
    } else {
        set deadline [im_freelance_calculate_deadline -multiple_freelance_package_ids $freelance_package_id_list]
    }
    
    set current_package_type [im_category_from_id -translate_p 0 $package_type_id]
    
    
    set previous_step_type_name ""
    set steps_and_packages [im_freelance_assignments_step_packages -freelance_package_ids $freelance_package_id_list]

    # We need to find out which step is first one for this kind of project
    set first_step_type [lindex [lindex $steps_and_packages 0] 0]
    set previous_step_type_name_temp $first_step_type

    foreach step_list $steps_and_packages {
        if {$current_package_type eq [lindex $step_list 0]} {
            set previous_step_type_name $previous_step_type_name_temp
        } else {
            set previous_step_type_name_temp [lindex $step_list 0]
        }
    }
    if {$current_package_type ne $first_step_type} {
        set previous_step_deadline [im_freelance_calculate_deadline -multiple_freelance_package_ids $freelance_package_id_list -specific_package_type $previous_step_type_name]
    } else {
        #db_1row project_info "select start_date as previous_step_deadline from im_projects 
              #where project_id = (select distinct project_id from im_freelance_packages 
              #where freelance_package_id in ([ns_dbquotelist $freelance_package_id_list]) limit 1)"
        set previous_step_deadline ""
    }
    

    # ---------------------------------------------------------------
    #  Rate calculation
    # ---------------------------------------------------------------

    # get first task_id of package, to later check for unit of measure
    set single_task_id [lindex $package_task_ids 0]
    
    # find the Unit of Measure id and also set its name
    db_1row get_task_info "select task_uom_id, p.project_id, tt.source_language_id, tt.target_language_id, subject_area_id from im_trans_tasks tt, im_projects p where p.project_id = tt.project_id and task_id = :single_task_id"
    set task_uom_name [im_category_from_id $task_uom_id]
    
    # Get the assignment units
    set assignment_units 0

    foreach task_id $package_task_ids {
    set task_units 0
    db_1row task_matrix "select task_units, match_x,match_rep,match100,match95,match85,match75,match50,match0,
                        match_perf,match_cfr,match_f95,match_f85,match_f75,match_f50,locked
                        from im_trans_tasks where task_id = :task_id"
    
    set task_units_matrix [im_trans_trados_matrix_calculate_helper $provider_id  $match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0  $match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $task_type]

    if {$task_units_matrix ne 0} {
        set task_units $task_units_matrix
    }
    if {$task_units eq ""} {
        set task_units 0
    }
    set assignment_units [expr $assignment_units + $task_units]
    }

    # Find the currency for the provider by looking at the prices
    set currency ""
    if {[im_table_exists "im_trans_prices"]} {
    db_0or1row currency "select currency, count(*) as num_prices from im_trans_prices where company_id = :provider_id group by currency order by num_prices desc limit 1"
    }
    
    if {$currency eq ""} {
    set currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
    }
    

    # Get the rate for the package!
    set price_task_type_id [db_string price_task_type_id "select aux_int2 from im_categories where category_id = :package_type_id" -default $package_type_id]
    set total_rate_info [im_translation_best_rate_helper  -provider_id $provider_id  -task_type_id $price_task_type_id  -subject_area_id $subject_area_id  -target_language_id $target_language_id  -source_language_id $source_language_id  -task_uom_id $task_uom_id  -currency $currency  -ignore_min_price_p 0  -task_sum $assignment_units]
    set total_billable_units [lindex $total_rate_info 0]
    set total_uom_id [lindex $total_rate_info 1]
    set total_rate [lindex $total_rate_info 2]
    
    # decide if we want to use 
    if {[im_uom_unit] eq $total_uom_id && $total_billable_units eq 1} {
    set total_amount $total_rate
    set min_price_used_p 1
    } else {
    set total_amount "[expr {double(round(100* $assignment_units * $total_rate))/100}]"
    set min_price_used_p 0
    }
    
    set komma ",\n"
    
    if {0 == $obj_ctr} { set komma "" }
    set single_row "$komma{\"freelance_package_id\":\"[ns_dbquotelist $freelance_package_id_list]\", \"deadline\":\"$deadline\", \"uom_id\":$task_uom_id, \"units\":\"$assignment_units\", \"rate\":\"$total_rate\"}"
    append packages_rate_and_deadlines $single_row
    
    incr obj_ctr

    set result "{\"success\": true, \"freelancer_id\":\"$freelancer_id\", \"freelancer_name\":\"[im_name_from_id $freelancer_id]\", \"package_type_id\":\"$package_type_id\", \"freelancer_company_id\":\"$provider_id\", \"freelance_package_id\":\"$freelance_package_id\", \"previous_step_deadline\":\"$previous_step_deadline\", \"deadline\":\"$deadline\", \"uom_id\":\"$task_uom_id\", \"assignment_units\":\"$assignment_units\", \"min_price_used_p\":$min_price_used_p, \"rate\":\"$total_rate\"}"
    im_rest_doc_return 200 "application/json" $result
    return
}

ad_proc -public im_rest_get_custom_sencha_create_assignment {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call which creates assignment for given @param feelancer_id and @param package_id
} {

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(freelance_package_id)]} {
        set freelance_package_id $query_hash(freelance_package_id)  
    } 

    if {[info exists query_hash(freelancer_id)]} {
        set freelancer_id $query_hash(freelancer_id)  
    } 

    if {[info exists query_hash(rate)]} {
        set rate $query_hash(rate)  
    } else {
        set rate ""
    }

    if {[info exists query_hash(units)]} {
        set units $query_hash(units)  
    } else {
        set units ""
    }

    if {[info exists query_hash(uom_id)]} {
        set uom_id $query_hash(uom_id)  
    } {
        set uom_id ""
    }

    if {[info exists query_hash(deadline)]} {
        set deadline $query_hash(deadline)  
    } else {
        set deadline ""
    }

    if {[info exists query_hash(comment)]} {
        set comment $query_hash(comment)  
    } {
        set comment "no comment"
    }

    if {[info exists query_hash(create_po)]} {
        set create_po_p $query_hash(create_po)  
    } else {
        set create_po_p 0
    }


    set existing_assignment_id [db_string assignment_exist "select assignment_id from im_freelance_assignments where freelance_package_id =:freelance_package_id and assignee_id =:freelancer_id limit 1" -default 0]
    if {$existing_assignment_id} {
        set assignment_id $existing_assignment_id
        db_dml update_assignment "update im_freelance_assignments set rate =:rate, assignment_units =:units, uom_id =:uom_id, end_date =:deadline where freelance_package_id =:freelance_package_id and assignee_id =:freelancer_id"
        db_dml update_assignment "update im_freelance_assignments set assignment_status_id = 4220 where freelance_package_id =:freelance_package_id and assignee_id =:freelancer_id"
        im_freelance_calculate_deadline -assignment_id $assignment_id -update_deadline_p 1
    } else {
        set assignment_type_id [db_string assignment_type_id "select package_type_id from im_freelance_packages where freelance_package_id =:freelance_package_id"]    
        set assignment_id [im_freelance_assignment_create -freelance_package_id $freelance_package_id -assignee_id $freelancer_id -rate $rate -assignment_units $units -uom_id $uom_id -end_date $deadline -assignment_type_id $assignment_type_id -ignore_min_price_p 1 -assignment_comment $comment]
        im_freelance_calculate_deadline -assignment_id $assignment_id -update_deadline_p 1

        db_dml update_assignment "update im_freelance_assignments set assignment_units =:units where assignment_id =:assignment_id"
    }


    if {$create_po_p} {
        set invoice_id [im_freelance_create_purchase_orders -assignment_id $assignment_id]
    } else {
        set invoice_id false
    }


    set result "{\"success\": true, \"assignment_id\":$assignment_id, \"invoice_id\":$invoice_id}"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_rest_get_custom_sencha_create_purchase_order {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call which creates assignment for given @param feelancer_id and @param package_id
} {



    if {[info exists query_hash(assignment_id)]} {
        set assignment_id $query_hash(assignment_id)  
    } else {
        set assignee_id ""
    }
    
    if {$assignee_id ne 0} {
        set invoice_id [im_freelance_create_purchase_orders -assignment_id $assignment_id]
    } else {
        set invoice_id false
    }
    

    set result "{\"success\": true, \"assignment_id\":$assignment_id, \"po_id\":$invoice_id}"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_rest_get_custom_sencha_delete_assignment {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call which changes status of assignment to 'deleted'
} {

    array set query_hash $query_hash_pairs
    
    if {[info exists query_hash(freelance_package_id)]} {
        set freelance_package_id $query_hash(freelance_package_id)  
    } 

    if {[info exists query_hash(freelancer_id)]} {
        set freelancer_id $query_hash(freelancer_id)  
    } 

    # changing assignment status to from 4220 (created) to 4230 (deleted)
    db_dml update_assignment "update im_freelance_assignments set assignment_status_id = 4230 where freelance_package_id =:freelance_package_id and assignee_id =:freelancer_id"

    set result "{\"success\": true}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_package_tasks {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call which returns list of tasks for given @param freelance_package_id
} {

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(freelance_package_id)]} {
        set freelance_package_id $query_hash(freelance_package_id)  
    } 

    # getting package task ids
    set single_task_list [list]
    set sql "select trans_task_id, task_name, task_uom_id, im_name_from_id(task_uom_id) as task_uom_name, task_units
        from im_freelance_packages_trans_tasks fptt, im_trans_tasks tt 
        where tt.task_id = fptt.trans_task_id 
        and fptt.freelance_package_id =:freelance_package_id
        and tt.task_status_id <> 372"

    db_foreach trans_tasks $sql {
        lappend single_task_list "{\"task_id\":$trans_task_id, \"task_name\":\"$task_name\", \"task_uom_id\":\"$task_uom_id\", \"task_uom_name\":\"$task_uom_name\", \"task_units\":\"$task_units\", \"active\":true}"
    }

    db_1row package_info "select project_id, package_type_id from im_freelance_packages where freelance_package_id =:freelance_package_id"
    
    # getting taks ids which are not yet attached to any package
    db_foreach unpackaged_tasks "select task_id, task_name, task_uom_id, im_name_from_id(task_uom_id) as task_uom_name, task_units from im_trans_tasks where project_id = :project_id 
        and task_status_id <> 372
        and task_id not in (select trans_task_id from im_freelance_packages_trans_tasks fptt, im_freelance_packages fp
            where fptt.freelance_package_id = fp.freelance_package_id
            and project_id = :project_id
            and package_type_id = :package_type_id )" {
            lappend single_task_list "{\"task_id\":$task_id, \"task_name\":\"$task_name\", \"task_uom_id\":\"$task_uom_id\", \"task_uom_name\":\"$task_uom_name\", \"task_units\":\"$task_units\", \"active\":true}"
    }

    set result "{\"success\": true, \"freelance_package_id\": $freelance_package_id, \n\"data\": \[\n[join $single_task_list ","]\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_udpate_package_tasks {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call which returns list of tasks for given @param freelance_package_id
} {

    array set query_hash $query_hash_pairs
    if {[info exists query_hash(freelance_package_id)]} {
        set freelance_package_id $query_hash(freelance_package_id)  
    } 

    if {[info exists query_hash(selected_tasks)]} {
        set selected_tasks $query_hash(selected_tasks)  
    } else {
        set selected_tasks [list]
    }

    # querying for package type id
    db_1row package_type_id "select package_type_id from im_freelance_packages where freelance_package_id =:freelance_package_id"
    

    # create list of newly selected task ids
    set newly_selected_task_list [list]
    foreach new_task_id [split $selected_tasks ","] {
        lappend newly_selected_task_list $new_task_id
    }

    # get current task ids
    set current_package_tasks_ids [db_list current_package_tasks_ids "select trans_task_id as task_id from im_freelance_packages_trans_tasks where freelance_package_id =:freelance_package_id"]

    #creating empty list which will store already used task ids
    set already_added_task_ids [list]

    # removing all tasks for this package
    db_dml delete_assignments "delete from im_freelance_packages_trans_tasks where freelance_package_id =:freelance_package_id"
    
    # looping thru tasks which existed before 'db_dml delete_assignments' 
    # and deciding whether insert them back or create new package for them
    if {[llength $newly_selected_task_list] eq 0} {

    } else {
        foreach current_package_task_id $current_package_tasks_ids {
            if {[lsearch $newly_selected_task_list $current_package_task_id] ne -1} {
                db_dml insert_assignment "insert into im_freelance_packages_trans_tasks values (:freelance_package_id, :current_package_task_id)"
            } else {
                set new_freelance_package_id [im_freelance_package_create -trans_task_ids [list $current_package_task_id] -package_type_id $package_type_id]
            }
            lappend already_added_task_ids $current_package_task_id
        }
        # looping thru new tasks (task without a package) which were not inserted yet
        foreach newly_selected_task_id $newly_selected_task_list {
            if {[lsearch $already_added_task_ids $newly_selected_task_id] eq -1} {
                db_dml insert_assignment "insert into im_freelance_packages_trans_tasks values (:freelance_package_id, :newly_selected_task_id)"
            }
        }
    }
    

    set result "{\"success\": true, \n\"data\": \[\n[join $selected_tasks ","]\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_freelancer_infobox {
    -freelancer_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on specific freelancer infobox
    
    @param freelancer_id id of freelancer

    @return portrait_url Portrait of freelancer
    @return freelancer_notes im_rest_get_custom_sencha_freelancer_notes Array of freelancer notes
    @return freelancer_skills im_rest_get_custom_sencha_freelancer_skills Array of freelancer skills
    @return freelancer_prices im_rest_get_custom_sencha_company_prices Array of freelancer prices (company prices)
    @return freelancer_workload im_rest_get_custom_sencha_freelancer_workload Array of freelancer workload
    @return freelancer_average_ratings im_rest_get_custom_sencha_freelancer_average_ratings Array of average ratings for the freelancer
    
} {

    # we se project_id to zero, because we can query for fl info without that var
    set project_id 0
    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)  
    } 

    set target_language_ids [list]

    if {$project_id ne 0} {

        set project_source_language_id [db_0or1row project_source_language_id "select source_language_id from im_projects where project_id =:project_id"]
        
        set target_language_sql "
            select
                l.language_id as target_language
            from
                im_target_languages l
            where
               project_id=:project_id
            "

        db_foreach select_target_languages $target_language_sql {
            lappend target_language_ids $target_language
        }

    } else {
        set project_source_language_id 0
        set target_language_sql "select skill_id from im_freelance_skills where user_id =:freelancer_id and (skill_type_id = 2002 or skill_type_id = 2000)"
        db_foreach select_target_languages $target_language_sql {
            lappend target_language_ids $skill_id
        }
    }

    set freelancer_info [db_0or1row freelancer_info "select first_names, last_name, email from cc_users where user_id =:freelancer_id"]
    set full_name "$first_names $last_name"
    set work_phone ""
    set cell_phone ""
    set freelancer_contact [db_0or1row freelancer_contact "select work_phone, cell_phone from users_contact where user_id=:freelancer_id"]
    set freelancer_company_id [im_translation_freelance_company -freelance_id $freelancer_id]

    set notes ""
    set obj_ctr 0

    # Portrait
    set user_fs_url "/intranet/download/user/$freelancer_id"
    set portrait_file [im_portrait_user_file $freelancer_id]
    set portrait_url "${user_fs_url}/$portrait_file"

    # Freelancer notes
    set freelancer_notes [im_rest_json_object_from_list -objects [im_rest_get_custom_sencha_freelancer_notes -object_id $freelancer_id -data_only_p 1]]

    # Freelancer skills
    set freelancer_skills [im_rest_json_object_from_list -objects [im_rest_get_custom_sencha_freelancer_skills -freelancer_id $freelancer_id -data_only_p 1]]

    # Freelancer prices
    set freelancer_prices [im_rest_json_object_from_list -objects [im_rest_get_custom_sencha_company_prices -company_id $freelancer_company_id -target_language_ids $target_language_ids -data_only_p 1]]

    # Freelancer workload - which means assignments with statuses: 4221 (Requested), 4222 (Accepted), 4224 (Work Started)
    set freelancer_workload [im_rest_json_object_from_list -objects [im_rest_get_custom_sencha_freelancer_workload -freelancer_id $freelancer_id -data_only_p 1]]
    
    # Freelancer average ratings
    set freelancer_average_ratings [im_rest_json_object_from_list -objects [im_rest_get_custom_sencha_freelancer_average_ratings -freelancer_id $freelancer_id -data_only_p 1]]

    set result "{\"success\": $project_source_language_id, \"object_id\": $freelancer_id, \"freelancer_id\": $freelancer_id, \"freelancer_company_id\": $freelancer_company_id, 
    \"freelancer_name\":\"$full_name\", \"freelancer_email\":\"$email\", \"work_phone\":\"$work_phone\", \"cell_phone\":\"$cell_phone\",\"portrait_url\":\"$portrait_url\", \n\"notes\": \[\n$notes\n\]\n, \n\"prices\": \[\n$freelancer_prices\n\]\n, \n\"freelancer_skills\": \[\n$freelancer_skills\n\]\n, \n\"freelancer_workload\": \[\n$freelancer_workload\n\]\n,\n\"freelancer_average_ratings\": \[\n$freelancer_average_ratings\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_freelancer_notes {
     -object_id
    { -data_only_p 0}
    { -path "" }
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} { 

    Handler for GET rest calls to get freelancer notes
    Can be also used as "normal" procedure which returnes data as list with data_only_p switch

    @param object_id in this case this one is freelancer_id

    @return note_id id of note
    @return note_type_id id of noty type
    @return assignment_type_name name of assignment type for returned workload
    @return date date of note_trimmed
    @return note parsed content of note
    @return content trimmed content of note
    @return creator_id id of user who created that note
    @return creator_name name of user who created that note

} {
    
    set freelancer_id $object_id

    set notes ""
    set notes_list [list]
    set obj_ctr 0
    set notes_sql "
    select  n.*,
        im_category_from_id(n.note_type_id) as note_type,
                o.last_modified,
                o.modifying_user, o.creation_user
    from    im_notes n, acs_objects o
    where   n.object_id = :freelancer_id
        and o.object_id = n.note_id
        order by o.last_modified desc
    "
    db_foreach note $notes_sql  {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        set note_date [lc_time_fmt $last_modified "%x" [lang::user::locale]]
        set note_trimmed [template::util::richtext::get_property html_value $note]
        set parsed_note [im_quotejson $note]
        set note_trimmed [im_quotejson $note_trimmed]
	if {$creation_user ne ""} {
	    set creator_name [im_name_from_id $creation_user]
	} else {
	    set creator_name ""
	}
        append notes "$komma{\"note_id\":$note_id, \"note_type\":\"$note_type\", \"freelancer_id\":$freelancer_id, \"date\":\"$note_date\",  \"note\":\"$parsed_note\", \"content\":\"$note_trimmed\", \"creator_id\":\"$creation_user\", \"creator_name\":\"$creator_name\"}"
        lappend notes_list [set notes_arr($obj_ctr) [list note_id $note_id note_type $note_type freelancer_id $freelancer_id date $note_date note $parsed_note content $note_trimmed creator_id $creation_user creator_name $creator_name]]
        incr obj_ctr
    }

    if {$data_only_p eq 1} {
        return $notes_list
    } else {
        set result "{\"success\": true, \"total\":$obj_ctr,\n\"data\": \[\n$notes\n\]\n}"
        im_rest_doc_return 200 "application/json" $result
        return
    }

}


ad_proc -public im_rest_get_custom_sencha_freelancer_skills {
     -freelancer_id
    { -data_only_p 0}
    { -path "" }
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} { 

    Handler for GET rest calls to get freelancer skills
    Can be also used as "normal" procedure which returnes data as list with data_only_p switch

    @param freelancer_id id of freelancer for whom we want to get skills

    @return skill_id id of freelancer skill
    @return skill_name name of freelancer skill
    @return skill_type_id type id of freelancer skill
    @return skill_type_name type name of freelancer skill
    @return confirmed_experience_id confirmed level id of freelancer skill
    @return confirmed_experience_name confirmed level name of freelancer skill

} {

    set freelancer_skills ""
    set freelancer_skills_list [list]
    set obj_ctr 0
    set freelancer_skills_sql "select skill_id, skill_type_id, confirmed_experience_id from im_freelance_skills where user_id =:freelancer_id"

    db_foreach freelancer_skill $freelancer_skills_sql {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        set skill_name [im_category_from_id $skill_id]
        set skill_type_name [im_category_from_id $skill_type_id]
        set confirmed_experience_name [im_category_from_id $confirmed_experience_id]
        append freelancer_skills "$komma{\"skill_id\":$skill_id, \"skill_name\":\"$skill_name\", \"skill_type_id\":\"$skill_type_id\", \"skill_type_name\":\"$skill_type_name\", \"confirmed_experience_id\": \"$confirmed_experience_id\", \"confirmed_experience_name\": \"$confirmed_experience_name\"}"
        lappend freelancer_skills_list [set freelancer_skills_arr($obj_ctr) [list skill_id $skill_id skill_name $skill_name skill_type_id $skill_type_id skill_type_name $skill_type_name confirmed_experience_id $confirmed_experience_id confirmed_experience_name $confirmed_experience_name]]
        incr obj_ctr      
    }

    if {$data_only_p eq 1} {
        return $freelancer_skills_list
    } else {
        set result "{\"success\": true, \"total\":$obj_ctr,\n\"data\": \[\n$freelancer_skills\n\]\n}"
        im_rest_doc_return 200 "application/json" $result
        return
    }

}


ad_proc -public im_rest_get_custom_sencha_company_prices {
     -company_id
     -target_language_ids
    { -data_only_p 0}
    { -path "" }
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {

    Handler for GET rest calls to get freelancer (company) prices
    Can be also used as "normal" procedure which returnes data as list with data_only_p switch

    @return uom_id unit of measure id for returned price
    @return uom_name unit of measure name for returned price
    @return source_language_id id of source language for returned price value
    @return source_language_name name of source language for returned price value
    @return target_language_id id of target language for returned price value
    @return target_language_name name of target language for returned price value
    @return subject_area_id id of subject area for returned price value
    @return subject_area_name name of subject area for returned price value
    @return task_type_id id of task type for returned price value
    @return task_type_name name of task type for returned prive value
    @return price price (value)
    @return min_price minimum price for assignment (final price can't be lower then this)

} {

    set prices ""
    set prices_list [list]
    set obj_ctr 0
    if {[llength $target_language_ids] > 0} {

        set prices_sql "select * from im_trans_prices where company_id =:company_id and target_language_id in([ns_dbquotelist $target_language_ids])"
    
        db_foreach price $prices_sql  {
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            set uom_name [im_category_from_id $uom_id]
            set source_language_name [im_category_from_id $source_language_id]
            set target_language_name [im_category_from_id $target_language_id]
            set subject_area_name [im_category_from_id $subject_area_id]
            set task_type_name [im_category_from_id $task_type_id]
            append prices "$komma{\"uom_id\":\"$uom_id\", \"uom_name\":\"$uom_name\", \"source_language_id\":\"$source_language_id\", \"source_language_name\":\"$source_language_name\", \"target_language_id\":\"$target_language_id\", \"target_language_name\":\"$target_language_name\", \"subject_area_id\": \"$subject_area_id\", \"subject_area_name\": \"$subject_area_name\", \"task_type_id\":\"$task_type_id\", \"task_type_name\":\"$task_type_name\", \"price\":\"$price\", \"min_price\":\"$min_price\"}"
            lappend prices_list [set prices_arr($obj_ctr) [list uom_id $uom_id uom_name $uom_name source_language_id $source_language_id source_language_name $source_language_name target_language_name $target_language_name subject_area $subject_area_id subject_area_name $subject_area_name task_type_id $task_type_id task_type_name $task_type_name price $price min_price $min_price]]
            incr obj_ctr
        }

        if {$data_only_p eq 1} {
            return $prices_list
        } else {
            set result "{\"success\": true, \"total\":$obj_ctr,\n\"data\": \[\n$prices\n\]\n}"
            im_rest_doc_return 200 "application/json" $result
            return
        }

    }
}


ad_proc -public im_rest_get_custom_sencha_freelancer_workload {
     -freelancer_id
    { -assignment_statuses_ids ""}
    { -data_only_p 0}
    { -path "" }
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {

    Handler for GET rest calls to get current freelancer workload.
    Can be also used as "normal" procedure which returnes data as list with 

    @param freelancer_id id of freelancer for whom we want to get workload)
    @param assignment_statuses_ids Optional list of ids
    @param data_only_p boolean param which decides if we want to use procedure as rest endpoint or "normal" proc which returns data in tcl list

    @return assignment_id id of assignment for returned workload
    @return assignment_type_id id of assignment type for returned workload
    @return assignment_type_name name of assignment type for returned workload
    @return project_id project id of assignment
    @return project_nr project number of assignment
    @return uom_id unit of measure id for returned workload
    @return uom_name unit of measure name for returned workload
    @return assignment_units number of units (counted as uom_id) for returned workload
    @return languages_combination combination of languages for returned workload (merged languages names into single string)
    @return start_date formatted start date for returned workload
    @return end_date formatted end date for returned workload


} {

    if {$assignment_statuses_ids eq ""} {
        set assignment_statuses_ids [list 4221 4222 4224]
    }

    set freelancer_workload ""
    set freelancer_workload_list [list]
    set obj_ctr 0
    set freelancer_workload_sql "select distinct fa.assignment_id, fa.uom_id, fa.assignment_units, fa.start_date, fa.end_date, fa.assignment_type_id, fp.project_id, p.project_nr, tt.source_language_id, tt.target_language_id from im_freelance_assignments fa, im_freelance_packages fp, im_freelance_packages_trans_tasks fptt, im_trans_tasks tt, im_projects p where fa.freelance_package_id = fp.freelance_package_id and fp.project_id = p.project_id and fp.freelance_package_id = fptt.freelance_package_id and fptt.trans_task_id = tt.task_id and fa.assignee_id =:freelancer_id and fa.assignment_status_id in ([ns_dbquotelist $assignment_statuses_ids]) order by end_date asc"
    db_foreach workload $freelancer_workload_sql {
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            set assignment_type_name [im_category_from_id $assignment_type_id]
            set languages_combination "[im_category_from_id $source_language_id]=>[im_category_from_id $target_language_id]"
            set start_date_formatted [lc_time_fmt $start_date "%d.%m.%Y"]
            set end_date_formatted [lc_time_fmt $end_date "%d.%m.%Y"]
            set uom_name [im_category_from_id $uom_id]
            append freelancer_workload "$komma{\"assignment_id\":$assignment_id, \"assignment_type_id\":$assignment_type_id, \"assignment_type_name\":\"$assignment_type_name\", \"project_id\":$project_id,\"project_nr\":\"$project_nr\",\"uom_id\":$uom_id,\"uom_name\":\"$uom_name\",\"assignment_units\":$assignment_units,\"languages_combination\":\"$languages_combination\", \"start_date\":\"$start_date_formatted\", \"end_date\":\"$end_date_formatted\"}"
            lappend freelancer_workload_list [set freelancer_workload_arr($obj_ctr) [list assignment_id $assignment_id assignment_type_id $assignment_type_name project_id $project_id project_nr $project_nr uom_id $uom_id uom_name $uom_name assignment_units $assignment_units languages_combination $languages_combination start_date $start_date_formatted end_date $end_date_formatted]]
            incr obj_ctr
    }

    if {$data_only_p eq 1} {
        return $freelancer_workload_list
    } else {
        set result "{\"success\": true, \"total\":$obj_ctr,\n\"data\": \[\n$freelancer_workload\n\]\n}"
        im_rest_doc_return 200 "application/json" $result
        return
    }

}



ad_proc -public im_rest_get_custom_sencha_freelancer_average_ratings {
     -freelancer_id
    { -data_only_p 0}
    { -path "" }
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls to get all files (with folders) structure for given project_id

    @param freelancer_id id of freelancer for whom we want average ratings (grouped by subject area)
    @param data_only_p boolean param which decides if we want to use procedure as rest endpoint or "normal" proc which returns data in tcl list

    @return subject_area_id subject area id of average rating
    @return subject_area_name subject name id of average rating
    @return quality_type_id quality type if of average rating
    @return quality_level_name quality level name of average rating
    @return average_rating avarage rating(value)
} {

    # Freelancer ratings groupped by subject_area
    set freelancer_average_ratings ""
    set freelancer_average_ratings_list [list]
    set obj_ctr 0
    set freelancer_ratings_sql "select avg(aux_int1) as average_rating, subject_area_id, quality_type_id from im_categories c, im_assignment_quality_report_ratings rr, im_assignment_quality_reports r, im_freelance_assignments fa where rr.report_id = r.report_id and c.category_id = rr.quality_level_id and r.assignment_id = fa.assignment_id and fa.assignee_id = :freelancer_id group by subject_area_id, quality_type_id"
    db_foreach rating $freelancer_ratings_sql {
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            set subject_area_name [im_category_from_id $subject_area_id]
            set quality_type_name [im_category_from_id $quality_type_id]
            append freelancer_average_ratings "$komma{\"subject_area_id\":$subject_area_id, \"subject_area_name\":\"$subject_area_name\", \"quality_type_id\":$quality_type_id, \"quality_type_name\":\"$quality_type_name\",\"average_rating\":$average_rating}"
            lappend freelancer_average_ratings_list [set freelancer_average_ratings_arr($obj_ctr) [list subject_area_id $subject_area_id subject_area_name $subject_area_name quality_type_id $quality_type_id quality_type_name $quality_type_name average_rating $average_rating]]
            incr obj_ctr
    }

    if {$data_only_p eq 1} {
        return $freelancer_average_ratings_list
    } else {
        set result "{\"success\": true, \"total\":$obj_ctr,\n\"data\": \[\n$freelancer_average_ratings\n\]\n}"
        im_rest_doc_return 200 "application/json" $result
        return
    }

}


ad_proc -public im_rest_get_custom_sencha_quality_reports_categories {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on specific freelancer infobox
} {
    
    set locale [lang::user::locale -user_id $rest_user_id -package_id [im_package_core_id]] 

    # Category types
    set category_types ""
    set obj_ctr 0
    set categories_to_be_rated_ids [parameter::get_from_package_key -package_key "sencha-assignment" -parameter "QualityTypeIDs" -default []]
    
    foreach category_id $categories_to_be_rated_ids {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        set qtip [im_category_string1 -category_id $category_id -locale $locale]
        append category_types "$komma{\"object_id\":$category_id,\"category_id\":$category_id, \"object_name\":\"[im_category_from_id -locale $locale $category_id]\", \"category_name\":\"[im_category_from_id -locale $locale $category_id]\", \"qtip\":\"[im_quotejson $qtip]\"}"
        incr obj_ctr
    }

    # Quality types
    set quality_types ""
    set obj_ctr 0
    set ratings_types_to_be_used [parameter::get_from_package_key -package_key "sencha-assignment" -parameter "QualityLevelIDS" -default []]
    set ratings_types_to_be_used [lreverse $ratings_types_to_be_used]
    foreach category_id $ratings_types_to_be_used {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append quality_types "$komma{\"object_id\":$category_id,\"category_id\":$category_id, \"object_name\":\"[im_category_from_id -locale $locale $category_id]\", \"category_name\":\"[im_category_from_id -locale $locale $category_id]\"}"
        incr obj_ctr
    }


    set result "{\"success\": true, \n\"quality_types\": \[\n$quality_types\n\]\n, \n\"category_types\": \[\n$category_types\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_project_ratings {
     -project_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls to get all created freelancer ratings for given project_id

    @param project_id ID of the project for which we want to get existing freelancer ratings
} {


    set freelancers_with_ratings ""
    set obj_ctr_fl 0

    set assignments_in_project [db_list assignments_in_project "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp where fa.freelance_package_id = fp.freelance_package_id and fp.project_id =:project_id"]
    foreach assignment_id $assignments_in_project {
        db_1row assignment_info "select assignee_id as freelancer_id, assignment_name from im_freelance_assignments where assignment_id =:assignment_id"
        set ratings ""
        set obj_ctr_ratings 0
        set sum 0
        set count_total_rated_categories 0
        set ratings_sql "select c.aux_int1 as quality_level_value, fa.assignment_id, qr.comment, qr.report_id, qrr.quality_level_id, qrr.quality_type_id from im_assignment_quality_reports qr, im_assignment_quality_report_ratings qrr, im_categories c, im_freelance_assignments fa where fa.assignment_id = qr.assignment_id and qr.report_id = qrr.report_id and c.category_id = qrr.quality_level_id and fa.assignment_id =:assignment_id"
        db_foreach rating $ratings_sql {
            set komma ",\n"
            if {0 == $obj_ctr_ratings} { set komma "" }
            set quality_level_name [im_category_from_id $quality_level_id]
            set quality_type_name [im_category_from_id $quality_type_id]
            set sum [expr $sum + $quality_level_value]
            append ratings "$komma{\"object_id\":$report_id, \"report_id\":$report_id, \"quality_type_id\":$quality_type_id, \"quality_type_name\":\"$quality_type_name\", \"quality_leve_id\":$quality_level_id, \"quality_level_name\":\"$quality_level_name\", \"quality_level_value\":\"$quality_level_value\"}"
            incr obj_ctr_ratings
            incr count_total_rated_categories
        }
        set komma ",\n"
        if {0 == $obj_ctr_fl} { set komma "" }
        set average 0
        if {$ratings ne ""} {
            set average [expr $sum / $count_total_rated_categories]
            db_1row project_info "select final_company_id as project_final_company_id, company_id as project_company_id from im_projects where project_id = :project_id"
            if {$project_final_company_id ne ""} {
                set company_id_to_display $project_final_company_id
            } else {
                set company_id_to_display $project_company_id
            }
            set company_name_to_display [db_string company_name_to_display "select company_name from im_companies where company_id =:company_id_to_display" -default 0]
            append freelancers_with_ratings "$komma{\"object_id\":$assignment_id,\"assignment_id\":$assignment_id,\"freelancer_id\":$freelancer_id, \"freelancer_name\":\"[im_name_from_user_id_helper $freelancer_id]\", \"assignment_name\":\"$assignment_name\",\"freelancer_company_id\":$company_id_to_display, \"freelancer_company_name\":\"$company_name_to_display\",\"total_rated_categories\":$count_total_rated_categories, \"sum\":$sum, \"average\":$average, \"comment\":\"[im_quotejson $comment]\",\n\"ratings\": \[\n$ratings\n\]\n}"
            incr obj_ctr_fl
        }
    }

    set result "{\"success\": true, \n\"data\": \[\n$freelancers_with_ratings\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_rest_get_custom_sencha_existing_assignment_ratings {
     -assignment_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls to get all created freelancer ratings for given project_id

    @param assignment_id of the assignment for which we want to get existing ratings
} {

    set assignment_existing_ratings ""
    set obj_ctr 0

    set existing_rating_sql "select fa.assignment_id, qrr.quality_level_id, qrr.quality_type_id from im_assignment_quality_reports qr, im_assignment_quality_report_ratings qrr, im_categories c, im_freelance_assignments fa where fa.assignment_id = qr.assignment_id and qr.report_id = qrr.report_id and c.category_id = qrr.quality_level_id and fa.assignment_id =:assignment_id"

    db_foreach ratin $existing_rating_sql {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append assignment_existing_ratings "$komma{\"quality_type_id\":$quality_type_id, \"quality_level_id\":$quality_level_id}"
        incr obj_ctr
    }

    set total $obj_ctr

    set result "{\"success\": true, \"total\":$total, \n\"data\": \[\n$assignment_existing_ratings\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}

