ad_page_contract {
    @author ND
} {
    assignment_id:integer,notnull
}

db_1row assignment_info "select assignee_id, freelance_package_id from im_freelance_assignments where assignment_id =:assignment_id"

set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]

db_1row package_info "select p.project_id, p.project_nr, p.project_lead_id, freelance_package_name, package_type_id from im_freelance_packages fp, im_projects p
    where freelance_package_id = :freelance_package_id
    and fp.project_id = p.project_id"

db_1row task_info "select im_name_from_id(source_language_id) as source_language,
 im_name_from_id(target_language_id) as target_language
  from im_trans_tasks where task_id = (
    select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id limit 1)"

set associated_task_ids [list]
set associated_tasks [list]
db_foreach tasks "select coalesce(task_filename,task_name) as task, task_id
  from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt
  where tt.task_id = fptt.trans_task_id
  and fptt.freelance_package_id = :freelance_package_id" {
  lappend associated_task_ids $task_id
  lappend associated_tasks $task
}

set associated_tasks_html ""
if {[llength $associated_tasks]>0} {
  append associated_tasks_html "[join $associated_tasks " - "]"
}


if {[im_assignment_is_rating_fl_possible -assignment_id $assignment_id]} {
    set display_rating true
} else {
    set display_rating false
}

set package_name $freelance_package_name
set task_type [im_category_from_id $package_type_id]


template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "[apm_package_url_from_key "sencha-assignment"]/freelancer-rating.js"
# im_company_permissions $user_id $company_id view read write admin

ns_log Notice "Called freelancer rating by [im_name_from_id [auth::get_user_id]] for $package_name and $assignment_id"

template::head::add_javascript -script "
       var serverParams = {
           serverUrl: '[ad_url]',
           user_id: '',
           auto_login: '',
           displayRating:$display_rating,
           assignment_id:$assignment_id,
           packageName:'$package_name',
           associatedTasks:'$associated_tasks',
           taskType:'$task_type'
       };
       serverParams.auth = '';
       serverParams.restUrl = serverParams.serverUrl + '/intranet-rest/';"
