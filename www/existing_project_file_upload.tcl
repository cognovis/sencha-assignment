ad_page_contract {
    upload_file
} {
	project_id:integer
}


set total_count 0
set total_count_with_errors 0
set tasks_names_with_errors ""
set all_files_uploaded_without_errors_p 1


set project_id [ns_queryget project_id]

set tmp_filename [ns_queryget upload_file.tmpfile]

set task_filename [ns_queryget upload_file]


# We need to determine if the file is wordcount or actual file. First we create "_p" variable 
set is_wordcount_file_p 0
# Then we check if file extension is actualy .xml
set splitted_task_filename [split $task_filename "."]
set count_splitted_parts [llength $splitted_task_filename]
set index_of_last_el [expr $count_splitted_parts - 1]
set extension [lindex $splitted_task_filename $index_of_last_el]
if {[string tolower $extension] eq "xml"} {
	# If file is an xml, we check file with chilkat
	set xml [new_CkXml]
	set success [CkXml_LoadXmlFile $xml $tmp_filename]
	if {[expr $success != 1]} {
        ns_log Notice "Chilkat: Error loading XML file"
	} else {
	    set count_tags [CkXml_NumChildrenHavingTag $xml "file"]
        if {$count_tags > 0} {
        	set is_wordcount_file_p 1
        }
	}
}


if {$is_wordcount_file_p eq 1} {
	 set trados_created_tasks [im_trans_trados_create_tasks -project_id $project_id -trados_analysis_xml $tmp_filename]
	 ns_log Notice "Trados created tasks: $trados_created_tasks"

	 set all_files_uploaded_without_errors_p 1
	 set total_count

     set result "{\"success\": true, \"all_files_uploaded_without_errors\":$all_files_uploaded_without_errors_p, \n\"total\": $total_count,\n\"message\": \"Worccount file loaded\",\n\"data\": \[\n{\"file\":\"$task_filename\",\"path\":\"${tmp_filename}.copy\"}\n\]\n, \"wordcount_file\":$is_wordcount_file_p, \"tasks_names_with_errors\":\"$tasks_names_with_errors\"}"

     im_rest_doc_return 200 "application/json" $result
return

} else {
	 
    set source_dir [im_trans_task_folder_path -project_id $project_id -folder_type "source"]
    file mkdir $source_dir
    file rename -force $tmp_filename "${source_dir}/$task_filename"
	    
    set project_type_id [db_string project_type "select project_type_id from im_projects where project_id = :project_id" -default ""]
	    
    callback im_project_after_file_upload -project_id $project_id -type_id $project_type_id -status_id [im_project_status_potential]
	   
    # Check for files in folder
    set uploaded_files [glob -nocomplain -directory $source_dir -type f *]

    set target_language_ids [im_target_language_ids $project_id]
    set project_type_id [db_string project_type_id "select project_type_id from im_projects where project_id =:project_id" -default 0]

	    
    foreach file $uploaded_files {
        set task_filename [file tail $file]
		    set count_created_tasks [db_string count_created_tasks "select count(*) from im_trans_tasks where project_id = :project_id and task_filename =:task_filename " -default 0]
		    set task_uom_id [im_uom_s_word] 
		    set new_task_id [im_task_insert $project_id $task_filename $task_filename 0 $task_uom_id $project_type_id $target_language_ids]
		    if {$new_task_id ne 0} {
		        incr total_count
		    } else {
		        if {$total_count_with_errors eq 0} {
                    set tasks_names_with_errors $task_filename
		        } else {
		            set tasks_names_with_errors "$tasks_names_with_errors, $task_filename"
		        }
		        set all_files_uploaded_without_errors_p 0
		        incr total_count_with_errors
		    }
    }



    set result "{\"success\": true, \"all_files_uploaded_without_errors\":$all_files_uploaded_without_errors_p, \n\"total\": $total_count,\n\"message\": \"File: Data loaded\",\n\"data\": \[\n{\"file\":\"$task_filename\",\"path\":\"${tmp_filename}.copy\"}\n\]\n, \"wordcount_file\":$is_wordcount_file_p, \"tasks_names_with_errors\":\"$tasks_names_with_errors\"}"

}


im_rest_doc_return 200 "application/json" $result
return

