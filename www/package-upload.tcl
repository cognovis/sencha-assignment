ad_page_contract {
    upload_file
} {
   assignment_id:integer,notnull
   {comment ""}
   {catrating1 ""}
   {ratings ""}
}

if {[im_assignment_is_rating_fl_possible -assignment_id $assignment_id]} {
	set previous_assignment_id [im_assignment_previous_stage_assignment_id -assignment_id $assignment_id]
     array set parsed_json [util::json::parse $ratings]
     set ratings_json_list $parsed_json(_array_)
     set save_ratings_p 1

} else {
     set save_ratings_p 0
}


set current_user_id [auth::require_login]

# Assignee & Package info 
db_1row assignment_info "select assignee_id, freelance_package_id from im_freelance_assignments where assignment_id =:assignment_id"

set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]

db_1row package_info "select p.project_id, p.project_nr, p.project_lead_id, freelance_package_name, package_type_id from im_freelance_packages fp, im_projects p
    where freelance_package_id = :freelance_package_id
    and fp.project_id = p.project_id"

db_1row task_info "select im_name_from_id(source_language_id) as source_language,
 im_name_from_id(target_language_id) as target_language
  from im_trans_tasks where task_id = (
  	select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id limit 1)"

set location [util_current_location]
if {$assignee_id ne ""} {
    set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
    set locale [lang::user::locale -user_id $assignee_id]
}

set type [im_category_from_id -translate_p 0 $package_type_id]
set internal_company_name [im_name_from_id [im_company_freelance]]

# This page is only called when we have a return_file.
set upload_folder [im_trans_task_folder \
		       -project_id $project_id \
		       -target_language $target_language \
		       -folder_type $type \
		       -return_package]

set package_file_type_id 600

set tmp_filename [ns_queryget upload_file.tmpfile]
set filename [ns_queryget upload_file]


set extension [file extension $filename]
		
set freelance_package_file_name "${freelance_package_name}$extension"
set folder "[cog::project::path -project_id $project_id]/${upload_folder}"
set file_path "$folder/$freelance_package_file_name"
set package_link [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]

if {![file exist $folder]} {
    file mkdir $folder
} 

file rename -force $tmp_filename $file_path

set package_exists_p [db_string exists_p "select 1 from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id = :package_file_type_id limit 1" -default 0]
if {!$package_exists_p} {
	# Mark the file as created (620)
	set ip_address [ad_conn peeraddr]
	set freelance_package_file_id [db_string create_file "select im_freelance_package_file__new(:current_user_id,:ip_address,:freelance_package_id,:freelance_package_file_name,:package_file_type_id,620,:file_path) from dual"]
} else {
	# Need to overwrite
	set package_file_sql "select freelance_package_file_id, file_path as old_file_path
		from im_freelance_package_files
		where freelance_package_id = :freelance_package_id and package_file_type_id = :package_file_type_id"
	db_foreach package_file $package_file_sql  {
		if {$old_file_path ne $file_path} {
			# delete the file
			file delete -force $old_file_path
			db_dml update_file "update im_freelance_package_files set file_path = :file_path,
				freelance_package_file_name = :freelance_package_file_name
				where freelance_package_file_id = :freelance_package_file_id"
		}
				
		db_dml update_object "update acs_objects set modifying_user = :current_user_id, last_modified = now() where object_id = :freelance_package_file_id"
	}

	
}

set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]
set package_link [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]


# Save ratings
if {$save_ratings_p eq 1} {
	foreach single_rating $ratings_json_list {
        array set rating_dataset [lindex $single_rating 1]
        im_assignment_quality_report_new -creation_user_id $current_user_id -assignment_id $previous_assignment_id -quality_type_id $rating_dataset(id_category) -quality_level_id $rating_dataset(value) -comment $comment
    }
} 

# Notify the PM about the uploads
set assignee_name [im_name_from_id $assignee_id]

im_freelance_notify \
    -object_id $assignment_id \
	-recipient_ids $project_lead_id \
	-message "[_ sencha-freelance-translation.notify_return_package]"
			
# Set the assignment status to work delivered (4225)
db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4225 where assignment_id = :assignment_id"
			
if {$comment ne ""} {
    # Create a message for the PM
	set log_id [im_freelance_message_send \
	    -sender_id $assignee_id \
		-recipient_ids $project_lead_id \
		-subject "$freelance_package_name Comment" \
		-body "$comment" \
		-context_id $assignment_id \
		-project_id $project_id]
}

# Notify the freelancer that the package was successfully uploaded
if {$assignee_id ne ""} {
    set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_package_uploaded_message_subject]"
	set body "[lang::message::lookup $locale sencha-freelance-translation.lt_package_uploaded_message_body]"
			
    im_freelance_notify \
	    -object_id $assignment_id \
		-recipient_ids $assignee_id \
		-message "$body"

		set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
		if {$signature ne ""} {
		    append body [template::util::richtext::get_property html_value $signature]
		}
	   
				intranet_chilkat::send_mail \
					-to_party_ids $assignee_id \
					-from_party_id $project_lead_id \
					-subject $subject \
					-body $body \
					-no_callback 
								
}


set redirect_url [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]


set result "{\"success\": true,\n\"total\": 1,\n\"message\": \"File: Data loaded\",\"redirect_url\": \"$redirect_url\",\n\"data\": \[\n{\"file\":\"$filename\",\"path\":\"${tmp_filename}.copy\"}\n\]\n}"

im_rest_doc_return 200 "application/json" $result

return
