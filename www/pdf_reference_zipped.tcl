ad_page_contract {
    @author MD
} {
	project_id:integer
}

# Project info will be useful
db_1row project_info "select project_nr, project_lead_id from im_projects where project_id = :project_id"

set files_to_be_zipped [list]
set project_folders_to_include [list [kolibri_trans_task_folder_helper -folder_type source] "Projektinfos"]

set original_project_folder [kolibri_trans_task_folder_helper -folder_type source]
set projektinfos_project_folder "Projektinfos"

# Setting project path
set project_path [cog::project::path -project_id $project_id]

# Creating temporary folder
set tmp_path [ns_tmpnam]
file mkdir $tmp_path

# Creating subfolders
set original_subfolder "$tmp_path/$original_project_folder"
set projektinfos_subfolder "$tmp_path/$projektinfos_project_folder"
file mkdir $original_subfolder
file mkdir $projektinfos_subfolder

# Copy Original files to subfolder
set original_folder_files [ad_find_all_files -max_depth 3 "$project_path/$original_project_folder" ]
foreach original_file $original_folder_files {
	set extension [file extension $original_file]
	# Use lowercased extension
	set extension [string tolower $extension]
	set extension [string trim $extension "."]
	# We first check if file is not '.idml' format. We ignore it in case it is
	if {$extension ne "idml"} {
		if {$extension eq "doc" || $extension eq "docx" || $extension eq "ppt" || $extension eq "pptx"} {
            set pdf_copy_result [intranet_oo::jodconvert -oo_file "$original_file" -output_file "$original_subfolder/.pdf"]
            # If conversion wasn't successful, we still add file in original format
            if {[lindex $pdf_copy_result 0] ne 1} {
                file copy $original_file $original_subfolder
            }
		} else {
            file copy $original_file $original_subfolder
		}
	}
}

# Copy Projekinfos files to subfolder
set projektinfos_folder_files [ad_find_all_files -include_dirs 1  "$project_path/$projektinfos_project_folder"]
foreach projektinfo_file $projektinfos_folder_files {
    # We need to check if code does not want to add folder itself. 
    # This is only happening if include_dirs is 1. In practice it is usually just ommiting first file (first loop)
	if {$projektinfo_file ne "$project_path/$projektinfos_project_folder"} {
	    file copy $projektinfo_file $projektinfos_subfolder
	} 
}

set outputfilename "sourcefiles_project-$project_nr.zip"

set new_zip_filename [intranet_chilkat::create_zip -directories $tmp_path -zipfile "$tmp_path/$outputfilename"] 



set outputheaders [ns_conn outputheaders]

ns_set cput $outputheaders "Content-Disposition" "attachment; filename=$outputfilename"
ns_returnfile 200 application/zip $new_zip_filename


# Delete whole temp folder which also contains generated .zip
file delete -force $tmp_path

