ad_page_contract {
    @author ND
} {
    project_id:integer,notnull
    assignee_id:integer,notnull
    {target_language_id ""}
}

set user_id [ad_maybe_redirect_for_registration]


template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "[apm_package_url_from_key "sencha-freelance-translation"]/sencha-project-assignments.js"
# im_company_permissions $user_id $company_id view read write admin

template::head::add_javascript -script "
       var serverParams = {
           serverUrl: '[ad_url]',
           user_id: '',
           auto_login: '',
           project_id: $project_id,
           assignee_id: $assignee_id
       };
       serverParams.restUrl = serverParams.serverUrl + '/intranet-rest/';"


set filter_js_list [list]
set counter 0

foreach v [list project_id skill_2002 skill_2000 skill_2014 skill_2024] {
    if {[exists_and_not_null $v]} {
        incr counter
        
        # Lists need to be transformed for the json to work
        if {[llength $v]>1} {
            set v [join $v ","]
        }
        lappend filter_js_list "
            \{
                   name: '$v',
                   values: \[ [set $v] \]
           \}
        "
    }
}

template::head::add_javascript -script "
    var filterParams = {
        project_id: '$project_id',
        skill_2002: '$target_language_id',
        minimumLanguageExperienceLevel:0,
        languageExperienceLevelFilterAlreadyUsed:false
    };
"
set projectFreelancersFilter_js "
   var projectFreelancersFilter = \{
       total: $counter,
       data: \[
               [join $filter_js_list ", \n"]
       \]
   \};"


template::head::add_javascript -script $projectFreelancersFilter_js