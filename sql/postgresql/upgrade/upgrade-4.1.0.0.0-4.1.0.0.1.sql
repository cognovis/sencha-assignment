-- 
-- packages/sencha-assignment/sql/postgresql/upgrade/upgrade-4.1.0.0.0-4.1.0.0.1.sql
-- 
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- @author <yourname> (<your email>)
-- @creation-date 2012-01-06
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/sencha-assignment/sql/postgresql/upgrade/upgrade-4.1.0.0.0-4.1.0.0.1.sql','');

-- Update colors for assignments
update im_categories set aux_string1 = '#61D9F2' where category_id = 4221;
update im_categories set aux_string1 = '#8CA65F' where category_id = 4222;
update im_categories set aux_string1 = '#FCC8C6' where category_id = 4223;
update im_categories set aux_string1 = '#C84860' where category_id = 4230;
